;;; Nani Project website
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of the Nani Project website.
;;;
;;; The Nani Project website is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; The Nani Project website is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with the Nani Project website.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (haunt asset)
	     (haunt reader)
	     (haunt site)
	     (haunt builder assets)
	     (haunt builder atom))

(use-modules (pages index)
	     (pages data)
	     (pages documentation)
	     (pages e404)
	     (pages feeds)
	     (pages mentions)
	     (pages blog))

(site #:title "Nani Project"
      #:domain "xana.lepiller.eu"
      #:default-metadata
      '((author . "Julien Lepiller")
        (email  . "webmaster@lepiller.eu"))
      #:readers (list sxml-reader)
      #:builders (append
                   (list (atom-feed #:filter get-posts)
                         (atom-feeds-by-tag #:filter get-posts)
                         (static-directory "css")
                         (static-directory "dicos")
                         (static-directory "images"))
                  blog-pages
                  page404
                  page-data
                  page-documentation
                  page-feeds
                  page-mentions
                  page-index))
