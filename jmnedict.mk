DICOS+=dicos/JMnedict.nani
DOWNLOADS+=dictionaries/JMnedict.xml

# Download JMdict dictionaries from ERDRG
dictionaries/JMnedict.xml:
	dl_filename="$(shell basename "$@")"; \
	wget ftp://ftp.edrdg.org/pub/Nihongo/"$$dl_filename".gz -O "$$dl_filename.gz"; \
	gunzip "$$dl_filename.gz"; \
	sed -i -e 's|&lt;|\&\&lt;;|g' -e 's|&gt;|\&\&gt;;|g' "$$dl_filename"; \
	sed -i -e 's|&\([^;]\+\);|\1|g' "$$dl_filename"; \
	mv "$$dl_filename" "$@"

dicos/JMnedict.nani: dictionaries/JMnedict.xml tools/jmnedict.scm $(DICO_MODULES)
	guile -L modules tools/jmnedict.scm build $< $@
