# keep po files, even if they are sometimes generated, and keep downloaded dictionaries around
.PRECIOUS: po/%.po dictionaries/%

all: site po/nani.pot

# To be filled by included files
# DICOS is the list of generated dictionaries
# DOWNLOADS is the list of downloaded files
DICOS=
DOWNLOADS=

include jibiki.mk
include jmdict.mk
include jmnedict.mk
include kanjidic.mk
include kanjivg.mk
include radicals.mk
include tatoeba.mk
include wadoku.mk

# Files that constitute the website
PAGES=blog.scm data.scm documentation.scm e404.scm feeds.scm index.scm mentions.scm
HAUNT_FILES= haunt.scm $(addprefix pages/, $(PAGES)) \
        tools/i18n.scm tools/theme.scm
SHA_DICOS=$(addsuffix .sha256, $(DICOS))
WEB_FILES= $(HAUNT_FILES) \
		   $(shell find css) $(shell find images) $(DICOS) $(SHA_DICOS) \
        dicos/list

# Guile modules used to build dictionaries
DICO_MODULES= \
  modules/nani/encoding/huffman.scm \
  modules/nani/encoding/parse.scm \
  modules/nani/encoding/serialize.scm \
  modules/nani/encoding/trie.scm \
  modules/nani/kanji/radk.scm \
  modules/nani/pitch/pitch.scm \
  modules/nani/pitch/wadoku.scm \
  modules/nani/result/frequency.scm \
  modules/nani/result/result.scm \
  modules/nani/result/jibiki.scm \
  modules/nani/result/jmdict.scm \
  modules/nani/result/wadoku.scm \

# Available languages
LANGS=fr uk zh_CN
MOFILES=$(addprefix po/, $(addsuffix /LC_MESSAGES/nani.mo, $(LANGS)))

dicos: $(DICOS) $(SHA_DICOS) dicos/list

site: $(MOFILES) $(WEB_FILES)
	haunt build
	rm -rf public.bak
	mv public public.bak
	mv site public
	touch site

download:
	@rm -rf dictionaries/*
	@$(MAKE) $(DOWNLOADS)

po/%/LC_MESSAGES/nani.mo: po/%.po
	@mkdir -p $$(dirname $@)
	msgfmt --output-file=$@ $<

po/nani.pot: $(HAUNT_FILES) tools/list.scm
	xgettext --keyword=_ --language=scheme --add-comments --sort-output --from-code UTF-8 -o $@ $^

%.sha256: %
	sha256sum $< | cut -f1 -d' ' > $@

dicos/list: $(DICOS) tools/list.scm $(MOFILES)
	rm -f $@
	guile -L modules -L . tools/list.scm $@ $(DICOS)

update-po:
	for f in po/*.po; do \
		msgmerge $$f po/nani.pot > $$f.tmp; \
		mv $$f.tmp $$f; \
	done
