# nani-website

This repository contains the code for the [website](https://nani.lepiller.eu)
of the Nani project. It uses haunt to build a static website and some guile code
to generate dictionaries and downloadable content for the project.

## Building and testing

### Dependencies

To build the website, you will need the following software:

* haunt
* guile

As well as more common software:

* make
* wget
* gzip
* sed, coreutils, grep, findutils, ...

The easiest way to do this is to use [guix](https://gnu.org/software/guix) and
install the necessary components in a temporary environment:

```bash
guix environment --ad-hoc guile haunt
```

### Testing locally

To test the website, build it with:

```bash
make
```

This can take quite a lot of time because it needs to build a lot data.

Then run a server with:

```bash
haunt serve
```

You can now connect to localhost to view the website.

### Updating dictionary data

Dictionaries are served by this website. The first time you build the website,
make will take care of downloading the necessary data and building the dictionary
files. Later, it will not download the files anymore, but you can update
them explicitly with:

```bash
make download
```

## Contributing

You can contribute in multiple ways:

### Report a bug, request improvements

Well, you can use the [Issues](https://framagit.org/nani-project/nani-website/issues) button on the left :)

### Translate the website

Translations are managed on Fedora's [Weblate](https://translate.fedoraproject.org/projects/nani/)
platform. Below are additional information in case you do not want to use
that platform.

If you want to translate the website, you can do so by downloading the `.po` file
corresponding to your language in the `po` folder, or the `po/nani.pot` file if there
is none yet. Then, you can use [poedit](https://poedit.net) for instance to load and
modify the file. Once you're done, save it and send it to me via an issue, a merge
request or simply by email, whatever is more convenient for you, dear translator :)

### Improve dictionary generation, add a new source, ...

Please discuss these in the issues for the [app](https://framagit.org/nani-project/nani-app),
not in this repository. Thank you :)
