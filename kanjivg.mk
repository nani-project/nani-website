DICOS+=dicos/kanjivg.nani
DOWNLOADS+=dictionaries/kanjivg

dictionaries/kanjivg:
	git clone --depth 1 https://github.com/KanjiVG/kanjivg $@

dicos/kanjivg.nani: dictionaries/kanjivg tools/kanjivg.scm $(DICO_MODULES)
	guile -L modules tools/kanjivg.scm build $< $@
