;;; Nani Project website
;;; Copyright © 2020 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of the Nani Project website.
;;;
;;; The Nani Project website is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; The Nani Project website is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with the Nani Project website.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nani result jibiki)
  #:use-module (ice-9 match)
  #:use-module (ice-9 rdelim)
  #:use-module (nani result frequency)
  #:use-module (nani result result)
  #:use-module (srfi srfi-9)
  #:use-module (sxml ssax)
  #:export (xml->results))

(define (sxml->string lst)
  (if (string? lst)
      lst
      (let loop ((lst lst) (result ""))
        (if (null? lst)
            result
            (loop
              (cdr lst)
              (match (car lst)
                (('lang . _) result)
                (#f result)
                ((? string? s) (string-append result s))
                ((? list? l) (loop l result))))))))

(define (gram->info gram)
  (match gram
    ("adjectifク" "adj. en ku")
    ("adjectif動" "adj. en na")
    ("adjectif動タリ" "adj. en na (tari)")
    ("adjectif動ナリ" "adj. en na (nari)")
    ("contraction" "contr.")
    ("counter" "compteur")
    ("numérique" "nombre")
    ("particule" "particule")
    ("postposition" "postpos.")
    ("verbe auxiliaire" "v. aux.")
    ("代 pronom" "pronom")
    ("副 adverbe" "adv.")
    ("副 onomatopée" "onom.")
    ("助動 verbe auxiliaire" "v. aux.")
    ("助数 compteur" "compteur")
    ("動 verbe" "v.")
    ("動 verbe intransitif" "v.i.")
    ("動 verbe transitif" "v.t.")
    ("名 nom" "n.")
    ("形 adjectif" "adj.")
    ("感" "interj.")
    ("感動 interjection" "interj.")
    ("成語 expression" "expr.")
    ("接尾 suffixe" "suf.")
    ("接尾 suffixe conjonctif" "suf. conj.")
    ("接続 conjonction" "conj.")
    ("接頭 préfixe" "pref.")
    ("連体" "adj. prénominal")
    ("連語 locution adverbiale" "loc. adv.")
    ("連語 locution conjonctive" "loc. conj.")
    ("連語 locution postpositive" "loc. post.")
    ("連語 mot composé" "mot composé")
    ("??" #f)
    ("" #f)))

(define (info->info i)
  (match (sxml->string i)
    ("abbreviation" "abbreviation")
    ("archaism" "archaïsme")
    ("children's language" "langage des enfants")
    ("colloquialism" "familier")
    ("derogatory" "terme de mépris")
    ("familiar language" "familier")
    ("female term or language" "langage des femmes")
    ("honorific or respectful (sonkeigo) language" "terme honorifique")
    ("humble (kenjougo) language" "terme humble")
    ("idiomatic expression" "idiom.")
    ("jocular, humorous term" "comique")
    ("male term or language" "langage des hommes")
    ("obscure term" "obscure")
    ("obsolete term" "obsolète")
    ("onomatopoeic or mimetic word" "onom.")
    ("polite (teineigo) language" "terme poli")
    ("proverb" "proverbe")
    ("sensitive" "sensible")
    ("slang" "argot")
    ("vulgar expression or word" "vulgaire")
    ("word usually written using kana alone" "habituellement écrit en kana")
    ("" #f)
    (() #f)
    (_ i)))

(define (sxml->ref lst)
  (let loop ((lst lst) (ref '()))
    (if (null? lst)
        ref
        (loop
          (cdr lst)
          (match (car lst)
            (#f ref)
            (('jpn . jpn) (cons jpn ref)))))))

(define (sxml->reading str)
  (make-reading '() '() (list str)))

(define (sxml->source lst)
  (let loop ((lst lst) (source (make-source '() #f "")))
    (if (null? lst)
        source
        (loop
          (cdr lst)
          (match (car lst)
            ((? string? s)
             (update-source source
               #:content (cons s (source-content source))))
            (('lang . l)
             (update-source source #:lang l))
            ((? list? l)
             (loop l source)))))))

(define (sxml->meaning lst)
  (let loop ((lst lst) (meaning (make-meaning '() '() '() '() '() "fr")))
    (if (null? lst)
        meaning
        (loop
          (cdr lst)
          (match (car lst)
            (#f meaning)
            (('n . _) meaning)
            (('info . info) (update-meaning meaning
                              #:infos (cons info (meaning-infos meaning))))
            (('content . c) (update-meaning meaning
                              #:glosses (append
                                          (map string-downcase
					       (string-split c #\,))
                                          (meaning-glosses meaning))))
            ((? source? s)
             (update-meaning meaning
               #:sources (cons s (meaning-sources meaning))))
            ((? string? _) meaning)
            ((? list? l) (loop l meaning)))))))

(define (sxml->result lst frq)
  (define result
    (let loop ((lst lst) (result (make-result 0 0 '() '() '())) (infos '())
                         (sources '()))
      (if (null? lst)
          (list result infos sources)
          (match (car lst)
            (#f (loop (cdr lst) result infos sources))
            (('id . _) (loop (cdr lst) result infos sources))
            (('identree . _) (loop (cdr lst) result infos sources))
            (('kanji . k)
             (loop (cdr lst)
                   (update-result result
                     #:kanjis (cons k (result-kanjis result)))
                   infos
                   sources))
            ((? reading? r)
             (loop
               (cdr lst)
               (update-result result
                 #:readings (cons r (result-readings result)))
               infos
               sources))
            ((? meaning? s)
             (loop
               (cdr lst)
               (update-result result
                 #:meanings (cons s (result-meanings result)))
               infos
               sources))
            ((? source? s)
             (loop (cdr lst) result infos (cons s sources)))
            (('info . info)
             (loop (cdr lst)
                   result
                   (cons info infos)
                   sources))
            ((? list? l)
             (match (loop l result infos sources)
               ((result infos sources)
                (loop (cdr lst) result infos sources))))))))

  (match result
    ((result infos sources)
     (if (or (and (null? (result-readings result))
                  (null? (result-kanjis result)))
             (null? (result-meanings result)))
         #f
         (let* ((word (if (null? (result-kanjis result))
                          (car (reading-readings
                                 (car (result-readings result))))
                          (car (result-kanjis result))))
                (score (frequency->score frq word))
                (meanings (result-meanings result))
                (meanings
                  (map
                    (lambda (s)
                      (update-meaning s
                        #:infos (append infos (meaning-infos s))
                        #:sources (append sources (meaning-sources s))))
                    meanings)))
           (update-result result
             #:score score
             #:meanings meanings))))))

(define (sxml->element lst elem frq)
  (let ((elem (match elem
                ((_ . elem) elem)
                (_ elem))))
    (match elem
      ('vedette-romaji #f)
      ('vedette-hiragana (sxml->reading (sxml->string lst)))
      ('vedette-jpn `(kanji . ,(sxml->string lst)))
      ('vedette lst)
      ('forme lst)
      ('domaine (if (null? lst) #f
                    (let ((info (sxml->string lst)))
                      (if (or (not info) (string-null? info))
                          #f
                          `(info . ,info)))))
      ('gram (let ((info (gram->info (sxml->string lst))))
               (if (or (not info) (string-null? info))
                   #f
                   `(info . ,info))))
      ('étiquettes lst)
      ('étiquettes-sens lst)
      ('texte-sens `(content . ,(sxml->string lst)))
      ('sens (sxml->meaning lst))
      ('bloc-gram lst)
      ('sémantique lst)
      ('article (sxml->result lst frq))
      ('registre (let ((info (sxml->string lst)))
                   (if (or (not info) (string-null? info))
                       #f
                       `(info . ,info))))
      ('littéralement
       (if (null? lst) #f (make-meaning
                            '() '() '() '("lit")
                            (list (sxml->string lst)) "fr")))
      ('etymologie (sxml->source lst))
      ('étymologie (sxml->source lst))
      ('note (let ((info (sxml->string lst)))
               (if (or (not info) (string-null? info)) #f `(info . ,info))))
      ('info (let ((info (info->info (sxml->string lst))))
               (if (or (not info) (string-null? info)) #f `(info . ,info))))
      ('dialecte (let ((info (sxml->string lst)))
                   (if (or (not info) (string-null? info)) #f `(info . ,info))))
      ('en lst)
      ('eng lst)
      ('xref (sxml->ref lst))

      ('vr #f)
      ('romaji #f)
      ('romajidx #f)
      ('rt #f)
      ('ruby #f)
      ('vj #f)
      ('jpn `(jpn . ,(sxml->string lst)))
      ('étiquettes-français #f)
      ('étiquettes-sous-sens #f)
      ('texte-sous-sens #f)
      ('sous-sens #f)
      ('sous-bloc-gram #f)
      ('français #f)
      ('exemple #f)
      ('exemples #f))))

(define (create-parser frq)
  (define results '())
  (ssax:make-parser
    NEW-LEVEL-SEED
    (lambda (elem-gi attributes namespaces expected-content seed)
      attributes)
    
    FINISH-ELEMENT
    (lambda (elem-gi attributes namespaces parent-seed seed)
      (cond
        ((equal? elem-gi 'volume)
         results)
        ((equal? elem-gi 'article)
         (let ((entry (sxml->result seed frq)))
           (set! results (cons entry results)))
         #f)
        (else
         (let* ((seed (reverse seed))
                (element (sxml->element seed elem-gi frq)))
           (cons element parent-seed)))))
    
    CHAR-DATA-HANDLER
    (lambda (string1 string2 seed)
      (cons (string-append string1 string2) seed))))

(define (xml->results port frq)
  (let ((results (filter result? ((create-parser frq) port '()))))
    (sort-results results)))
