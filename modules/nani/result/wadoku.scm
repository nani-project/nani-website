;;; Nani Project website
;;; Copyright © 2019, 2024 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of the Nani Project website.
;;;
;;; The Nani Project website is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; The Nani Project website is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with the Nani Project website.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nani result wadoku)
  #:use-module (ice-9 match)
  #:use-module (ice-9 rdelim)
  #:use-module (nani result frequency)
  #:use-module (nani result result)
  #:use-module (srfi srfi-9)
  #:use-module (sxml ssax)
  #:export (xml->results))

(define (usg->infos lst)
  (let loop ((infos '()) (lst lst))
    (if (null? lst)
        infos
        (loop
          (match (car lst)
            (('reg . reg) (cons reg infos))
            (('type . type)
             (if (equal? type "abrev")
                 (cons type infos)
                 infos))
            ((? string? info) (cons info infos))
            (_ infos))
          (cdr lst)))))

(define (ruigo->ref lst)
  (let loop ((ref #f) (lst lst))
    (match lst
      (() ref)
      ((('id id) lst ...)
       (loop id lst))
      (((a . b) lst ...)
       (loop (loop ref (list (list a b))) lst))
      (((? list? l) lst ...)
       (loop (loop ref l) lst)))))

(define (merge-meanings s1 s2)
  (update-meaning s2
    #:references (append (meaning-references s1) (meaning-references s2))
    #:infos (append (meaning-infos s1) (meaning-infos s2))
    #:glosses (append (meaning-glosses s1) (meaning-glosses s2))))

(define (sxml->meaning lst)
  (let loop ((meaning (make-meaning '() '() '() '() '() "ger")) (lst lst))
    (if (null? lst)
      meaning
      (loop
        (match (car lst)
          (#f meaning)
          ((? meaning? s)
           (merge-meanings s meaning))
          ((? source? s)
           (update-meaning meaning #:sources (cons s (meaning-sources meaning))))
          (('ref (? string? r))
           (update-meaning meaning #:references (cons r (meaning-references meaning))))
          (('info (? string? r))
           (update-meaning meaning #:infos (cons r (meaning-infos meaning))))
          (('infos (? string? r))
           (update-meaning meaning #:infos (cons r (meaning-infos meaning))))
          (('infos (? list? r))
           (update-meaning meaning #:infos (append r (meaning-infos meaning))))
          (('trans (? string? r))
           (update-meaning meaning #:glosses (cons (string-downcase r)
                                               (meaning-glosses meaning))))
          (('related . _) meaning)
          (('transcr . _) meaning)
          (('pitch . _) meaning)
          (((? symbol? s) v)
           (throw 'unknown-symbol s v))
          ((? list? l) (loop meaning l))
          ((? string? _) meaning))
        (cdr lst)))))

;; TODO
(define (sxml->ref lst)
  (define (sxml->ref-name lst)
    (let loop ((result #f) (lst lst))
      (if (null? lst)
        result
        (loop
          (match (car lst)
            (('id . id) id)
            (((? symbol? s) . _) result)
            (((? symbol? s) _) result)
            ((? list? l) (loop result l))
            (_ result))
          (cdr lst)))))
  (let ((ref (sxml->ref-name lst)))
    (if (string? ref)
        `(ref ,ref)
        (throw 'no-ref ref 'from lst))))

(define (sxml->source lst)
  (let loop ((source (make-source '() #f "")) (lst lst))
    (if (null? lst)
      source
      (loop
        (match (car lst)
          (('impli impli) (update-source source #:lang impli))
          (('foreign foreign)
           (update-source source
             #:content (if (list? foreign)
                           (append foreign (source-content source))
                           (cons foreign (source-content source)))))
          ((? list? l) (loop source l))
          (_ source))
        (cdr lst)))))

(define (sxml->reading lst)
  (let loop ((reading (make-reading '() '() '())) (lst lst))
    (if (null? lst)
      reading
      (loop
        (match (car lst)
          (('reading r)
           (update-reading reading #:readings (cons r (reading-readings reading))))
          (_ reading))
        (cdr lst)))))

(define (sxml->result sxml frq)
  (define (sxml->result-aux sxml)
    (let loop ((result (make-result 0 0 '() '() '())) (last-source #f) (lst sxml))
      (if (null? lst)
        result
        (match (car lst)
          (('kanji kanji)
           (loop
             (update-result result #:kanjis (cons kanji (result-kanjis result)))
             last-source (cdr lst)))
          ((? reading? reading)
           (loop
             (update-result result #:readings (cons reading (result-readings result)))
             last-source (cdr lst)))
          ((? meaning? meaning)
           (loop
             (update-result result
               #:meanings
               (cons
                 (if last-source
                     (update-meaning meaning
                       #:sources (cons last-source (meaning-sources meaning)))
                     meaning)
                 (result-meanings result)))
             last-source (cdr lst)))
          ((? list? l)
           (loop (loop result last-source l) last-source (cdr lst)))
          (_ (loop result last-source (cdr lst)))))))
  (let* ((result (sxml->result-aux sxml))
         (word (if (null? (result-kanjis result))
                   (car (reading-readings (car (result-readings result))))
                   (car (result-kanjis result))))
         (score (frequency->score frq word))
         (meanings (result-meanings result)))
    (update-result result
      #:score score
      #:meanings meanings)))

(define (meishi->info lst)
  (match (assoc-ref lst 'suru)
    ("both" "N., mit suru intrans. od. trans.")
    ("intrans" "N., mit suru intrans.")
    ("trans" "N., mit suru trans.")))

(define (keiyoushi->info lst)
  (cond
    ((equal? (assoc-ref lst 'ku) "true")
     "Adj. auf ‑ku")
    ((equal? (assoc-ref lst 'shiku) "true")
     "Adj. auf -shiku")))

(define (keiyoudoushi->info lst)
  (if (equal? (assoc-ref lst 'nari) "true")
      "Na.‑Adj. mit nari"
      "Na.‑Adj. mit na od. no"))

(define (fukushi->info lst)
  (define (get attr)
    (match (assoc-ref lst attr)
      ("false" #f)
      ("true" #t)
      (v v)))
  (string-append
    "Adv"
    (if (get 'ni) ", mit ni und Adn. mit naru" "")
    (if (get 'to)
        (if (get 'naru)
            ", mit to und Adn. mit taru"
            ", mit to")
        "")
    (match (get 'suru)
      ("intrans" ", mit suru intrans. V.")
      ("trans" ", mit suru trans. V.")
      ("both" ", mit suru trans. od. intrans. V.")
      (_ ""))))

(define (doushi->info lst)
  (define transitivity-str
    (match (assoc-ref lst 'transitivity)
      ("intrans" "intrans.")
      ("trans" "trans.")
      ("both" "trans. od. intrans.")))

  (define onbin (assoc-ref lst 'onbin))

  (match (assoc-ref lst 'level)
    ("kuru" (string-append "unregelm. " transitivity-str " V. auf ka"))
    ("ra" (string-append transitivity-str " V. auf -ra"))
    ("suru" (string-append transitivity-str " V. auf -suru"))
    ("1e" (string-append "1‑st. " transitivity-str " V. auf -e"))
    ("1i" (string-append "1‑st. " transitivity-str " V. auf -i"))
    ("2e" (string-append "2‑st. " transitivity-str " V. auf -e bzw. -u"))
    ("2i" (string-append "2‑st. " transitivity-str " V. auf -i bzw. -u"))
    ("4" (string-append "4‑st. " transitivity-str " V."))
    ("5" (string-append "5-st. " transitivity-str " V."
            (match (assoc-ref lst 'godanrow)
              ("ba" (if onbin
                        " auf -ba mit regelm. Nasal-Onbin = ‑nde"
                        " auf -ba"))
              ("ga" (if onbin
                        " auf -ga mit regelm. i-Onbin = ‑ide"
                        " auf -ga"))
              ("ka_i_yu" (if onbin
                             " auf -ka mit Geminaten-Onbin = ‑tte"
                             " auf -ka"))
              ("ka" (if onbin
                        " auf -ka mit i-Onbin = ‑ite"
                        " auf -ka"))
              ("ma" (if onbin
                        " auf -ma regelm. Nasal-Onbin = ‑nde"
                        " auf -ma"))
              ("na" (if onbin
                        " auf -na mit regelm. Nasal-Onbin = ‑nde"
                        " auf -na"))
              ("ra_i" (if onbin
                          " auf -ra, Sonderform mit Renyō·kei ‑i"
                          " auf -ra"))
              ("ra" (if onbin
                        " auf -ra mit regelm. Geminaten-Onbin = ‑tte"
                        " auf -ra"))
              ("sa" " auf -sa")
              ("ta" (if onbin
                        " auf -ta mit regelm. Geminaten-Onbin = ‑tte"
                        " auf -ta"))
              ("wa" (if onbin
                        " auf -[w]a mit Geminaten-Onbin = ‑tte"
                        " auf -[w]a"))
              ("wa_o" (if onbin
                          " auf -[w]a mit u-Onbin = ‑ō/ūte"
                          " auf -[w]a"))
              (#f ""))))))

(define (gram->info lst)
  (map
    (lambda (gram)
      (match gram
        (('daimeishi) "Pron.")
        (('doushi doushi ...) (doushi->info doushi))
        (('fukujoshi) "adv. Part.")
        (('fukushi) "Adv.")
        (('fukushi fukushi ...) (fukushi->info fukushi))
        (('fuujoshi) "")
        (('heiritsujoshi) "parallel Part.")
        (('jodoushi) "Hilfsv.")
        (('joshi) "Part.")
        (('kakarijoshi) "Themenpart.")
        (('kakujoshi) "Kasuspart.")
        (('kandoushi) "Interj.")
        (('kanji) "Kanji")
        (('keiyoudoushi) "Na.‑Adj.")
        (('keiyoudoushi keiyoudoushi ...) (keiyoudoushi->info keiyoudoushi))
        (('keiyoushi) "Adj.")
        (('keiyoushi keiyoushi ...) (keiyoushi->info keiyoushi))
        (('meishi) "N.")
        (('meishi meishi ...) (meishi->info meishi))
        (('prefix) "Präf.")
        (('rengo) "Zus.")
        (('rentaishi) "Adn")
        (('setsuzokujoshi) "konjunktionale Part.")
        (('setsuzokushi) "Konj.")
        (('shuujoshi) "satzbeendende Part.")
        (('specialcharacter) "Sonderzeichen")
        (('suffix) "Suff.")
        (('wordcomponent) "Wortkomp.")))
    lst))

(define (sxml->string lst)
  (define (sub-loop loop infos result lst l)
    (let ((result (loop infos result l)))
      (if (list? result)
          (loop (append infos (filter list? result))
                (apply string-append (filter string? result))
                lst)
          (loop infos result lst))))

  (let loop ((infos '()) (result "") (lst lst))
    (match lst
      (() (if (null? infos)
              result
              (append infos (list result))))
      ((? string? s)
       (loop infos (string-append result s) '()))
      (((? string? s) lst ...)
       (loop infos (string-append result s) lst))
      ((('prior . _) lst ...)
       (loop infos result lst))
      ((('options . _) lst ...)
       (loop infos result lst))
      ((('firstname . _) lst ...)
       (loop infos result lst))
      ((('ausn . _) lst ...)
       (loop infos result lst))
      ((('lang . _) lst ...)
       (loop infos result lst))
      ((('meta . _) lst ...)
       (loop infos result lst))
      ((('genki . genki) lst ...)
       ;(loop (cons `(info ,genki) infos) result lst))
       (loop infos result lst))
      ((('jlpt . jlpt) lst ...)
       (loop (cons `(info ,(string-append "jlpt-" jlpt)) infos) result lst))
      ((('ref . (? string? ref)) lst ...)
       (loop (cons `(ref ,ref) infos) result lst))
      ((('ref (? string? ref)) lst ...)
       (loop (cons `(ref ,ref) infos) result lst))
      ((('jap l) lst ...)
       (sub-loop loop infos result lst l))
      ((('foreign l) lst ...)
       (sub-loop loop infos result lst l))
      ((('transcr l) lst ...)
       (sub-loop loop infos result lst l))
      ((((? symbol? s) v) lst ...)
       (throw 'unsupported-symbol s v))
      ((((? symbol? s) . v) lst ...)
       (throw 'unsupported-symbol-pair s v))
      (((? list? l) lst ...)
       (sub-loop loop infos result lst l)))))

(define (sxml->element lst elem frq)
  (match elem
    ('orth (let ((kanji (filter string? lst)))
             (if (null? kanji)
                 #f
                 `(kanji
                    ,(string-filter
                       (lambda (c)
                         ;; Remove characters that interfere with kanji
                         ;; based search
                         (not (member c '(#\△ #\（ #\） #\｛ #\｝ #\〈 #\〉
                                          #\× #\╳))))
                       (car kanji))))))
    ('count #f)
    ('entry (sxml->result lst frq))
    ('hira `(reading ,(car lst)))
    ('hatsuon `(hatsuon ,(car lst)))
    ('accent `(pitch ,(car lst)))
    ('reading (sxml->reading lst))
    ('form lst)
    ('impli `(impli ,(car lst)))
    ('text (let loop ((text "") (lst lst))
             (match lst
               (() text)
               ((('hasPrecedingSpace . _) lst ...)
                (loop (string-append " " text) lst))
               ((('hasFollowingSpace . _) lst ...)
                (string-append (loop text lst) " "))
               (((? string? s) lst ...)
                (loop (string-append text s) lst)))))
    ('famn (sxml->string lst))
    ('expl (sxml->string lst))
    ('expli (filter list? lst))
    ('abbrev (filter list? lst))
    ('token (filter string? lst))
    ('tr (sxml->string lst))
    ('transcr `(transcr ,lst))
    ('trans
      (append (filter list? lst)
              (map (lambda (s) `(trans ,s)) (filter string? lst))))
    ('jap `(jap ,lst))
    ('emph lst)
    ('title (filter string? lst))
    ('transl (filter string? lst))
    ('topic (filter string? lst))
    ('iron (filter string? lst))
    ('specchar (filter string? lst))
    ('scientif (filter string? lst))
    ('wikide #f)
    ('wikija #f)
    ('link #f)
    ('ref (sxml->ref lst))
    ('sref (sxml->ref lst))
    ('etym (sxml->source lst))
    ('literal (sxml->string (list "„" (sxml->string lst) "“")))
    ('def (sxml->string (list "(" (sxml->string lst) ")")))
    ('date (sxml->string (list "(" (sxml->string lst) ")")))
    ('birthdeath (sxml->string (list "(" (sxml->string lst) ")")))
    ('descr (sxml->string (list "(" (sxml->string lst) ")")))
    ('bracket (sxml->string (list "[" (sxml->string lst) "]")))
    ('foreign (if (null? lst) #f `(foreign ,(car lst))))
    ('seasonword `(info ,(string-append "season: " (assoc-ref lst 'type))))
    ('usg `(infos . ,(usg->infos lst)))
    ('sense (sxml->meaning lst))
    ('steinhaus (let ((ref (sxml->string lst)))
                  (if (string? ref)
                      `(ref ,(sxml->string lst))
                      (throw 'not-steinhaus ref))))
    ('pos '()); TODO: actually find what info to use
    ('wordcomponent (cons 'wordcomponent lst))
    ('meishi (cons 'meishi lst))
    ('setsuzokushi (cons 'setsuzokushi lst))
    ('daimeishi (cons 'daimeishi lst))
    ('doushi (cons 'doushi lst))
    ('kandoushi (cons 'kandoushi lst))
    ('keiyoudoushi (cons 'keiyoudoushi lst))
    ('keiyoushi (cons 'keiyoushi lst))
    ('fukushi (cons 'fukushi lst))
    ('rengo (cons 'rengo lst))
    ('suffix (cons 'suffix lst))
    ('prefix (cons 'prefix lst))
    ('kanji (cons 'kanji lst))
    ('rentaishi (cons 'rentaishi lst))
    ('specialcharacter (cons 'specialcharacter lst))
    ('joshi (cons 'joshi lst))
    ('fukujoshi (cons 'fukujoshi lst))
    ('kakujoshi (cons 'kakujoshi lst))
    ('kakarijoshi (cons 'kakarijoshi lst))
    ('shuujoshi (cons 'shuujoshi lst))
    ('setsuzokujoshi (cons 'setsuzokujoshi lst))
    ('jokeiyoushi (cons 'jokeiyoushi lst))
    ('jodoushi (cons 'jodoushi lst))
    ('heiritsujoshi (cons 'heiritsujoshi lst))
    ('ruigos lst)
    ('ruigo (ruigo->ref lst))
    ('gramGrp (gram->info lst))))

(define (create-parser frq)
  (define results '())
  (ssax:make-parser
    NEW-LEVEL-SEED
    (lambda (elem-gi attributes namespaces expected-content seed)
      attributes)
    
    FINISH-ELEMENT
    (lambda (elem-gi attributes namespaces parent-seed seed)
      (let ((elem-gi (match elem-gi
                       ((_ . elem) elem)
                       (_ elem-gi))))
        (cond
          ((equal? elem-gi 'entries)
           results)
          ((equal? elem-gi 'entry)
           (let ((entry (sxml->result seed frq)))
             (set! results (cons entry results)))
           #f)
          (else
           (let* ((seed (reverse seed))
                  (element (sxml->element seed elem-gi frq)))
             (cons element parent-seed))))))
    
    CHAR-DATA-HANDLER
    (lambda (string1 string2 seed)
      (cons (string-append string1 string2) seed))))

(define (xml->results port frq)
  (let ((results (filter result? ((create-parser frq) port '()))))
    (sort-results results)))
