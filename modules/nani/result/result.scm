;;; Nani Project website
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of the Nani Project website.
;;;
;;; The Nani Project website is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; The Nani Project website is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with the Nani Project website.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nani result result)
  #:use-module (ice-9 binary-ports)
  #:use-module (ice-9 match)
  #:use-module (rnrs bytevectors)
  #:use-module (srfi srfi-9)
  #:use-module (nani encoding serialize)
  #:use-module (nani encoding trie)
  #:use-module (nani encoding huffman)
  #:export (%RESULT-VERSION
            make-result
            result?
            result-position
            result-position-set!
            result-score
            result-kanjis
            result-readings
            result-meanings
            
            make-reading
            reading?
            reading-kanjis
            reading-info
            reading-readings
            
            make-meaning
            meaning?
            meaning-references
            meaning-limits
            meaning-sources
            meaning-infos
            meaning-glosses
            meaning-language
            
            make-source
            source?
            source-content
            source-wasei?
            source-lang
            
            update-result
            update-reading
            update-meaning
            update-source

            serialize-result result-size
            serialize-reading reading-size
            serialize-meaning meaning-size
            serialize-source source-size

            serialize-dictionary
            dictionary-entry-count
            sort-results))

(define %RESULT-VERSION "NANI_JMDICT003")

(define-record-type result
  (make-result position score kanjis readings meanings)
  result?
  (position result-position result-position-set!) ; integer
  (score result-score) ; integer
  (kanjis result-kanjis) ; string-list
  (readings result-readings) ; reanding-list
  (meanings result-meanings)) ; meaning-list

(define-record-type reading
  (make-reading kanjis info readings)
  reading?
  (kanjis reading-kanjis) ; string-list
  (info reading-info) ; string-list
  (readings reading-readings)) ; string-list

(define-record-type meaning
  (make-meaning references limits sources infos glosses language)
  meaning?
  (references meaning-references) ; string-list
  (limits meaning-limits) ; string-list
  (sources meaning-sources) ; source-list
  (infos meaning-infos) ; string-list
  (glosses meaning-glosses) ; string-list
  (language meaning-language)) ; string

(define-record-type source
  (make-source content wasei? lang)
  source?
  (content source-content) ; string-list
  (wasei? source-wasei?) ; boolean
  (lang source-lang)) ; string

(define* (update-result result
           #:key (score (result-score result))
                 (kanjis (result-kanjis result))
                 (readings (result-readings result))
                 (meanings (result-meanings result)))
  (make-result (result-position result) score kanjis readings meanings))

(define* (update-reading reading
           #:key (kanjis (reading-kanjis reading))
                 (info (reading-info reading))
                 (readings (reading-readings reading)))
  (make-reading kanjis info readings))

(define* (update-meaning meaning
           #:key (references (meaning-references meaning))
                 (limits (meaning-limits meaning))
                 (sources (meaning-sources meaning))
                 (infos (meaning-infos meaning))
                 (glosses (meaning-glosses meaning))
                 (language (meaning-language meaning)))
  (make-meaning references limits sources infos glosses language))

(define* (update-source source
           #:key (content (source-content source))
                 (wasei? (source-wasei? source))
                 (lang (source-lang source)))
  (make-source content wasei? lang))

;; Note how sources are not compressed, that's because they contain more weird characters
;; and represent only a very small fraction of the content, even after compression.
(define (serialize-source source pos bv)
  (when (not (source? source)) (throw 'not-source source))
  (let* ((pos ((serialize-list serialize-string) (source-content source) pos bv))
         (pos (serialize-boolean (source-wasei? source) pos bv))
         (pos (serialize-string (source-lang source) pos bv)))
    pos))
(define (source-size source)
  (when (not (source? source)) (throw 'not-source source))
  (+ ((list-size string-size) (source-content source))
     (boolean-size (source-wasei? source))
     (string-size (source-lang source))))

(define (serialize-reading reading-huffman-code)
  (lambda (reading pos bv)
    (when (not (reading? reading)) (throw 'not-reading reading))
    (let* ((pos ((serialize-list serialize-string) (reading-kanjis reading) pos bv))
           (pos ((serialize-list serialize-string) (reading-info reading) pos bv))
           (pos ((serialize-list (serialize-huffman-string reading-huffman-code))
                 (reading-readings reading) pos bv)))
      pos)))
(define (reading-size reading-huffman-code)
  (lambda (reading)
    (when (not (reading? reading)) (throw 'not-reading reading))
    (+ ((list-size string-size) (reading-kanjis reading))
       ((list-size string-size) (reading-info reading))
       ((list-size (huffman-string-size reading-huffman-code)) (reading-readings reading)))))

(define (serialize-meaning meaning-huffman-code)
  (lambda (meaning pos bv)
    (when (not (meaning? meaning)) (throw 'not-meaning meaning))
    (let* ((pos ((serialize-list serialize-string) (meaning-references meaning) pos bv))
           (pos ((serialize-list serialize-string) (meaning-limits meaning) pos bv))
           (pos ((serialize-list serialize-source) (meaning-sources meaning) pos bv))
           (pos ((serialize-list (serialize-huffman-string meaning-huffman-code))
                 (meaning-infos meaning) pos bv))
           (pos ((serialize-list (serialize-huffman-string meaning-huffman-code))
                 (meaning-glosses meaning) pos bv))
           (pos (serialize-string (meaning-language meaning) pos bv)))
      pos)))
(define (meaning-size meaning-huffman-code)
  (lambda (meaning)
    (when (not (meaning? meaning)) (throw 'not-meaning meaning))
    (+ ((list-size string-size) (meaning-references meaning))
       ((list-size string-size) (meaning-limits meaning))
       ((list-size source-size) (meaning-sources meaning))
       ((list-size (huffman-string-size meaning-huffman-code))
        (meaning-infos meaning))
       ((list-size (huffman-string-size meaning-huffman-code))
        (meaning-glosses meaning))
       (string-size (meaning-language meaning)))))

(define (serialize-result kanji-huffman-code reading-huffman-code meaning-huffman-code)
  (lambda (result pos bv)
    (when (not (result? result)) (throw 'not-result result))
    (result-position-set! result pos)
    (let* ((pos ((serialize-list (serialize-huffman-string kanji-huffman-code))
                 (result-kanjis result) pos bv))
           (pos ((serialize-list (serialize-reading reading-huffman-code))
                 (result-readings result) pos bv))
           (pos ((serialize-list (serialize-meaning meaning-huffman-code))
                 (result-meanings result) pos bv))
           (pos (serialize-char (result-score result) pos bv)))
      pos)))
(define (result-size kanji-huffman-code reading-huffman-code meaning-huffman-code)
  (lambda (result)
    (when (not (result? result)) (throw 'not-result result))
    (+ ((list-size (huffman-string-size kanji-huffman-code)) (result-kanjis result))
       ((list-size (reading-size reading-huffman-code)) (result-readings result))
       ((list-size (meaning-size meaning-huffman-code)) (result-meanings result))
       (char-size (result-score result)))))

;; creating tries
(define (make-key key)
  (apply append
    (map
      (lambda (c)
        (list (quotient c 16) (modulo c 16)))
      (bytevector->u8-list (string->utf8 key)))))

(define (make-kanji-trie results)
  (let ((trie (make-empty-trie)))
    (let loop ((results results) (i 0))
      (if (null? results)
        (compress-trie trie)
        (begin
          (for-each
            (lambda (key)
              (add-to-trie! trie (make-key key) i))
            (result-kanjis (car results)))
          (loop (cdr results) (+ i 1)))))))

(define (make-reading-trie results)
  (let ((trie (make-empty-trie)))
    (let loop ((results results) (i 0))
      (if (null? results)
        (compress-trie trie)
        (begin
          (for-each
            (lambda (reading)
              (for-each
                (lambda (key)
                  (add-to-trie! trie (make-key key) i))
                (reading-readings reading)))
            (result-readings (car results)))
          (loop (cdr results) (+ i 1)))))))

(define (make-meaning-trie results)
  (let ((trie (make-empty-trie)))
    (let loop ((results results) (i 0))
      (if (null? results)
        (compress-trie trie)
        (begin
          (for-each
            (lambda (meaning)
              (for-each
                (lambda (key)
                  (add-to-trie! trie (make-key key) i))
                (meaning-glosses meaning)))
            (result-meanings (car results)))
          (loop (cdr results) (+ i 1)))))))

(define (update-trie-pos! trie results)
  (let* ((vals (trie-vals trie))
         (vals (map (lambda (i) (result-position (array-ref results i))) vals)))
    (trie-vals-set! trie vals))
  (for-each
    (match-lambda
      ((char . child)
       (update-trie-pos! child results)))
    (trie-transitions trie)))

(define (serialize-dictionary results)
  (define kanji-huffman
    (let ((kanjis (apply append (map result-kanjis results))))
      (create-huffman kanjis)))
  (define kanji-huffman-code (huffman->code kanji-huffman))
  (define reading-huffman
    (let* ((readings (apply append (map result-readings results)))
           (readings (apply append (map reading-readings readings))))
      (create-huffman readings)))
  (define reading-huffman-code (huffman->code reading-huffman))
  (define meaning-huffman
    (let* ((meanings (apply append (map result-meanings results)))
           (infos (apply append (map meaning-infos meanings)))
           (glosses (apply append (map meaning-glosses meanings))))
      (create-huffman (append infos glosses))))
  (define meaning-huffman-code (huffman->code meaning-huffman))

  (define (trie-node-size trie)
    (apply + 1 (map trie-node-size (map cdr (trie-transitions trie)))))

  (let* ((header (string->utf8 %RESULT-VERSION))
         (header-size (bytevector-length header))
         (pointers (make-bytevector 16 0))
         (kanji-huffman-bv (serialize-huffman kanji-huffman))
         (kanji-huffman-size (bytevector-length kanji-huffman-bv))
         (reading-huffman-bv (serialize-huffman reading-huffman))
         (reading-huffman-size (bytevector-length reading-huffman-bv))
         (meaning-huffman-bv (serialize-huffman meaning-huffman))
         (meaning-huffman-size (bytevector-length meaning-huffman-bv))
         (serialize-trie (serialize-trie serialize-int int-size))
         (trie-size (trie-size int-size))
         (kanji-trie (make-kanji-trie results))
         (kanji-trie-size (trie-size kanji-trie))
         (reading-trie (make-reading-trie results))
         (reading-trie-size (trie-size reading-trie))
         (meaning-trie (make-meaning-trie results))
         (meaning-trie-size (trie-size meaning-trie))
         (trie-sizes (+ kanji-trie-size reading-trie-size meaning-trie-size))
         (results-size
           ((list-size (result-size kanji-huffman-code reading-huffman-code
                                    meaning-huffman-code)
                       #:size? #f)
            results))
         (huffman-size (+ reading-huffman-size meaning-huffman-size kanji-huffman-size))
         (pos-kanji (+ header-size 16 kanji-huffman-size reading-huffman-size
                       meaning-huffman-size results-size 4))
         (bv (make-bytevector (+ header-size 16 kanji-huffman-size
                                 reading-huffman-size
                                 meaning-huffman-size
                                 results-size 4 trie-sizes))))
    (format #t "Number of nodes in kanjis: ~a~%"
      (trie-node-size kanji-trie))
    (format #t "Number of nodes in readings: ~a~%"
      (trie-node-size reading-trie))
    (format #t "Number of nodes in meanings: ~a~%"
      (trie-node-size meaning-trie))
    (format #t "First trie is at ~a~%" pos-kanji)
    ((serialize-list (serialize-result kanji-huffman-code reading-huffman-code
                                       meaning-huffman-code)
                     #:size? #f)
     results (+ header-size 16 huffman-size) bv)
    ;; Serializing results also updated result-pos for each of them
    (let ((results (list->array 1 results)))
      (update-trie-pos! kanji-trie results)
      (update-trie-pos! reading-trie results)
      (update-trie-pos! meaning-trie results))
    ;; number of entries
    (serialize-int (length results) (+ header-size 16 huffman-size results-size)
                   bv)
    (let* ((results (list->array 1 results))
           (pos pos-kanji)
           (pos (serialize-trie kanji-trie pos bv))
           (pos-reading pos)
           (pos (serialize-trie reading-trie pos bv))
           (pos-meaning pos)
           (pos (serialize-trie meaning-trie pos bv)))
      ;; Point to the trie structures
      (bytevector-u32-set!
        pointers 0
        (+ header-size 16 huffman-size results-size (int-size 0))
        (endianness big))
      ;; point to the kanji trie structure
      (bytevector-u32-set! pointers 4 pos-kanji (endianness big))
      ;; point to the reading trie structure
      (bytevector-u32-set! pointers 8 pos-reading (endianness big))
      ;; point to the meaning trie structure
      (bytevector-u32-set! pointers 12 pos-meaning (endianness big))
      ;; copy to result bytevector
      (bytevector-copy! header 0 bv 0 header-size)
      (bytevector-copy! pointers 0 bv header-size 16)
      (bytevector-copy! kanji-huffman-bv 0 bv (+ header-size 16) kanji-huffman-size)
      (bytevector-copy! reading-huffman-bv 0 bv
                        (+ header-size 16 kanji-huffman-size)
                        reading-huffman-size)
      (bytevector-copy! meaning-huffman-bv 0 bv
                        (+ header-size 16 kanji-huffman-size reading-huffman-size)
                        meaning-huffman-size)
      ;; give some feedback on the size of file's structures
      (format #t "huffmans are ~a bytes long~%" huffman-size)
      (format #t "results is ~a bytes long~%" results-size)
      (format #t "kanji trie is ~a bytes long~%" kanji-trie-size)
      (format #t "reading trie is ~a bytes long~%" reading-trie-size)
      (format #t "meaning trie is ~a bytes long~%" meaning-trie-size)
      bv)))

(define (dictionary-entry-count file)
  (call-with-input-file file
    (lambda (port)
      (let* ((header (utf8->string (get-bytevector-n port 14)))
             (pointers (get-bytevector-n port 16))
             (end-pos (bytevector-u32-ref pointers 0 (endianness big))))
        (seek port (- end-pos 4) SEEK_SET)
        (bytevector-u32-ref (get-bytevector-n port 4) 0 (endianness big))))))

(define (sort-results results)
  (define (get-string res)
    (if (null? (result-kanjis res))
        (car (reading-readings (car (result-readings res))))
        (car (result-kanjis res))))
  (sort
    results
    (lambda (a b)
      (cond
        ((> (result-score a) (result-score b)) #t)
        ((= (result-score a) (result-score b))
         (string<? (get-string a) (get-string b)))
        ((< (result-score a) (result-score b)) #f)))))
