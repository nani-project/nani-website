;;; Nani Project website
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of the Nani Project website.
;;;
;;; The Nani Project website is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; The Nani Project website is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with the Nani Project website.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nani result frequency)
  #:use-module (ice-9 rdelim)
  #:export (load-frequency
            frequency->score))

(define (load-frequency file)
  (call-with-input-file file
    (lambda (port)
      (let loop ((frq '()) (i 1))
        (let* ((line (%read-line port))
               (line (car line)))
          (if (eof-object? line)
            frq
            (loop (cons (cons line i) frq) (+ i 1))))))))

(define (frequency->score frq word)
  (let ((freq (assoc-ref frq word)))
    (cond
      ((not freq) 0)
      ((< freq 501) 32)
      ((< freq 1001) 16)
      ((< freq 2001) 8)
      ((< freq 5001) 4)
      ((< freq 10001) 2)
      ((< freq 20001) 1)
      (else 0))))
