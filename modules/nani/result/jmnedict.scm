;;; Nani Project website
;;; Copyright © 2024 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of the Nani Project website.
;;;
;;; The Nani Project website is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; The Nani Project website is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with the Nani Project website.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nani result jmnedict)
  #:use-module (ice-9 match)
  #:use-module (ice-9 rdelim)
  #:use-module (nani result frequency)
  #:use-module (nani result result)
  #:use-module (sxml simple)
  #:use-module (sxml ssax)
  #:export (load-dic xml->results))

(define (load-dic file)
  (xml->sxml (call-with-input-file file read-string)))

(define (sxml->reading lst)
  (let loop ((reading (make-reading '() '() '())) (lst lst))
    (if (null? lst)
      reading
      (loop
        (match (car lst)
          (('reading r) (update-reading reading #:readings (cons r (reading-readings reading))))
          (((? symbol? s) v) (throw 'unknown-content s v))
          ((? string? _) reading))
        (cdr lst)))))

(define (sxml->meaning lst)
  (let loop ((meaning (make-meaning '() '() '() '() '() "eng")) (lst lst))
    (if (null? lst)
      meaning
      (loop
        (match (car lst)
          (('info (? string? r)) (update-meaning meaning #:infos (cons r (meaning-infos meaning))))
          (('gloss (? string? r)) (update-meaning meaning
                                    #:glosses
                                    (cons (string-downcase r)
                                          (meaning-glosses meaning))))
          (((? symbol? s) v) (throw 'unknown-content s v))
          ((? list? l) (loop meaning l))
          ((? string? _) meaning))
        (cdr lst)))))

(define (sxml->result lst)
  (let ((result
        (let loop ((result (make-result 0 0 '() '() '())) (lst lst))
          (if (null? lst)
            result
            (loop
              (match (car lst)
                (('kanji kanji)
                 (update-result result #:kanjis (cons kanji (result-kanjis result))))
                ((? reading? r)
                 (update-result result #:readings (cons r (result-readings result))))
                ((? meaning? s)
                 (update-result result #:meanings (cons s (result-meanings result))))
                ((? string? _) result))
              (cdr lst))))))
    result))

(define (sxml->element lst elem)
  (match elem
    ('ent_seq "")
    ('re_pri "")
    ('keb (if (and (= (length lst) 1) (string? (car lst)))
              `(kanji ,(car lst))
              (throw 'invalid-keb lst)))
    ('reb (if (and (= (length lst) 1) (string? (car lst)))
              `(reading ,(car lst))
              (throw 'invalid-reb lst)))
    ('r_ele (sxml->reading lst))
    ('k_ele (car (filter list? lst)))
    ('name_type (if (and (= (length lst) 1) (string? (car lst)))
                    `(info ,(car lst))
                    (throw 'invalid-name_type lst)))
    ('trans_det (if (and (= (length lst) 1) (string? (car lst)))
                    `(gloss ,(car lst))
                    (throw 'invalid-name_type lst)))
    ('trans (sxml->meaning lst))
    ('entry (sxml->result lst))))

(define (create-parser)
  (define results '())
  (ssax:make-parser
    NEW-LEVEL-SEED
    (lambda (elem-gi attributes namespaces expected-content seed)
      (map
        (match-lambda
          ((k . v) (list k v)))
        (filter
          (match-lambda
            ((k . v) (not (member k '(g_type)))))
          attributes)))

    FINISH-ELEMENT
    (lambda (elem-gi attributes namespaces parent-seed seed)
      (cond
        ((equal? elem-gi 'JMnedict)
         results)
        ((equal? elem-gi 'entry)
         (let ((entry (sxml->result seed)))
           (set! results (cons entry results)))
         #f)
        (else
         (let* ((seed (reverse seed))
                (element (sxml->element seed elem-gi)))
           (cons element parent-seed)))))

    CHAR-DATA-HANDLER
    (lambda (string1 string2 seed)
      (cons (string-append string1 string2) seed))))

(define (xml->results port)
  (filter result? ((create-parser) port '())))
