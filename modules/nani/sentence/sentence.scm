;;; Nani Project website
;;; Copyright © 2022 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of the Nani Project website.
;;;
;;; The Nani Project website is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; The Nani Project website is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with the Nani Project website.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nani sentence sentence)
  #:use-module (ice-9 binary-ports)
  #:use-module (ice-9 match)
  #:use-module (rnrs bytevectors)
  #:use-module (srfi srfi-9)
  #:use-module (nani encoding serialize)
  #:use-module (nani encoding huffman)
  #:use-module (nani encoding trie)
  #:use-module (mecab mecab)
  #:export (%SENTENCE-VERSION
            make-sentence
            sentence?
            sentence-jpn
            sentence-trans
            sentence-tags
            sentence-audio

            serialize-sentence
            serialize-sentence-dictionary
            sentence-dictionary-entry-count))

(define %SENTENCE-VERSION "NANI_SENTENCE001")

(define-record-type <sentence>
  (make-sentence jpn trans tags audio)
  sentence?
  (position sentence-position sentence-position-set!)
  (jpn      sentence-jpn)
  (trans    sentence-trans)
  (tags     sentence-tags)
  (audio    sentence-audio))

(define (serialize-audio filename pos bv)
  (let ((size (if filename (stat:size (stat filename)) 0)))
    (let ((pos (serialize-u16 size pos bv)))
      (if filename
        (let ((fbv (call-with-input-file filename get-bytevector-all)))
          (bytevector-copy! fbv 0 bv pos size)
          (+ pos size))
        pos))))
(define (audio-size filename)
  (let ((size (if filename (stat:size (stat filename)) 0)))
    (+ (u16-size size) size)))

(define (serialize-sentence jpn-huffman trans-huffman)
  (lambda (sentence pos bv)
    (when (not (sentence? sentence)) (throw 'not-sentence sentence))
    (sentence-position-set! sentence pos)
    (let* ((pos ((serialize-huffman-string jpn-huffman)
                 (sentence-jpn sentence) pos bv))
           (pos ((serialize-huffman-string trans-huffman)
                 (sentence-trans sentence) pos bv))
           (pos ((serialize-list (serialize-huffman-string trans-huffman))
                 (sentence-tags sentence) pos bv))
           (pos (serialize-audio (sentence-audio sentence) pos bv)))
      pos)))
(define (sentence-size jpn-huffman trans-huffman)
  (lambda (sentence)
    (when (not (sentence? sentence)) (throw 'not-sentence sentence))
    (+ ((huffman-string-size jpn-huffman) (sentence-jpn sentence))
       ((huffman-string-size trans-huffman) (sentence-trans sentence))
       ((list-size (huffman-string-size trans-huffman)) (sentence-tags sentence))
       (audio-size (sentence-audio sentence)))))

(define (make-key key)
  (apply append
    (map
      (lambda (c)
        (list (quotient c 16) (modulo c 16)))
      (bytevector->u8-list (string->utf8 key)))))

(define (update-trie-pos! trie sentences)
  (let* ((vals (trie-vals trie))
         (vals (map (lambda (i) (sentence-position (array-ref sentences i))) vals)))
    (trie-vals-set! trie vals))
  (for-each
    (match-lambda
      ((char . child)
       (update-trie-pos! child sentences)))
    (trie-transitions trie)))

(define (serialize-sentence-dictionary sentences)
  (define jpn-huffman
    (let ((jpn (map sentence-jpn sentences)))
      (create-huffman jpn)))
  (define jpn-huffman-code (huffman->code jpn-huffman))

  (define trans-huffman
    (let ((trans (map sentence-trans sentences))
          (tags (apply append (map sentence-tags sentences))))
      (create-huffman (append trans tags))))
  (define trans-huffman-code (huffman->code trans-huffman))

  (define (make-sentence-trie sentences)
    (let ((trie (make-empty-trie))
          (tagger (mecab-new-tagger '())))
      (let loop ((sentences sentences) (i 0))
        (if (null? sentences)
          (begin
            (mecab-destroy tagger)
            (compress-trie trie))
          (begin
            (for-each
              (lambda (key)
                (add-to-trie! trie (make-key key) i))
              (mecab-words tagger (sentence-jpn (car sentences))))
            (loop (cdr sentences) (+ i 1)))))))

  (define (trie-node-size trie)
    (apply + 1 (map trie-node-size (map cdr (trie-transitions trie)))))

  (let* ((header (string->utf8 %SENTENCE-VERSION))
         (header-size (bytevector-length header))
         (pointers (make-bytevector 4 0))
         (jpn-huffman-bv (serialize-huffman jpn-huffman))
         (jpn-huffman-size (bytevector-length jpn-huffman-bv))
         (trans-huffman-bv (serialize-huffman trans-huffman))
         (trans-huffman-size (bytevector-length trans-huffman-bv))
         (serialize-trie (serialize-trie serialize-int int-size))
         (trie-size (trie-size int-size))
         (sentence-trie (make-sentence-trie sentences))
         (sentence-trie-size (trie-size sentence-trie))
         (sentences-size
           ((list-size (sentence-size jpn-huffman-code trans-huffman-code)
                       #:size? #f)
            sentences))
         (huffman-size (+ jpn-huffman-size trans-huffman-size))
         (pos-trie (+ header-size 4 jpn-huffman-size trans-huffman-size
                      sentences-size 4))
         (bv (make-bytevector (+ header-size 4 jpn-huffman-size
                                 trans-huffman-size sentences-size 4
                                 sentence-trie-size))))
    (format #t "Number of nodes in trie: ~a~%" (trie-node-size sentence-trie))
    ((serialize-list (serialize-sentence jpn-huffman-code trans-huffman-code)
                     #:size? #f)
     sentences (+ header-size 4 huffman-size) bv)
    ;; Serializing sentences also updated sentence-pos for each of them
    (let ((sentences (list->array 1 sentences)))
      (update-trie-pos! sentence-trie sentences))
    ;; number of entries
    (serialize-int (length sentences) (+ header-size 4 huffman-size sentences-size)
                   bv)
    (let* ((sentences (list->array 1 sentences))
           (pos pos-trie)
           (pos (serialize-trie sentence-trie pos bv)))
      ;; point to the trie structure
      (bytevector-u32-set!
        pointers 0
        (+ header-size 4 huffman-size sentences-size (int-size 0))
        (endianness big))
      ;; copy to result bytevector
      (bytevector-copy! header 0 bv 0 header-size)
      (bytevector-copy! pointers 0 bv header-size 4)
      (bytevector-copy! jpn-huffman-bv 0 bv (+ header-size 4) jpn-huffman-size)
      (bytevector-copy! trans-huffman-bv 0 bv (+ header-size 4 jpn-huffman-size)
                        trans-huffman-size)
      ;; gide some feedback on the size of file's structures
      (format #t "huffmans are ~a bytes long~%" huffman-size)
      (format #t "sentences are ~a bytes long~%" sentences-size)
      (format #t "trie is ~a bytes long~%" sentence-trie-size)
      bv)))

(define (sentence-dictionary-entry-count file)
  (call-with-input-file file
    (lambda (port)
      (let* ((header (utf8->string (get-bytevector-n port 16)))
             (pointers (get-bytevector-n port 4))
             (end-pos (bytevector-u32-ref pointers 0 (endianness big))))
        (seek port (- end-pos 4) SEEK_SET)
        (bytevector-u32-ref (get-bytevector-n port 4) 0 (endianness big))))))
