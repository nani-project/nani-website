;;; Nani Project website
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of the Nani Project website.
;;;
;;; The Nani Project website is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; The Nani Project website is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with the Nani Project website.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nani encoding huffman)
  #:use-module (ice-9 match)
  #:use-module (rnrs bytevectors)
  #:export (create-huffman
            huffman->code
            huffman-encode
            huffman-decode
            serialize-huffman
            serialize-huffman-string huffman-string-size))

(define (add-occurence occ char)
  (let* ((o (assoc-ref occ char))
         (o (+ (if o o 0) 1)))
    (assoc-set! occ char o)))

(define (add-occurences occ str)
  (let loop ((lst (append (string->list str) (list #\nul))) (occ occ))
    (if (null? lst)
      occ
      (loop (cdr lst) (add-occurence occ (car lst))))))

(define (get-occurences string-list)
  (let loop ((lst string-list) (occ '()))
    (if (null? lst)
      occ
      (loop (cdr lst) (add-occurences occ (car lst))))))

(define (order tree)
  (if (null? tree)
    tree
    (add-in-order (car tree) (order (cdr tree)))))

(define (add-in-order elem tree)
  (if (null? tree)
    (list elem)
    (let ((elem-pos (cdr elem))
          (tree-pos (cdr (car tree))))
      (if (> elem-pos tree-pos)
        (cons (car tree) (add-in-order elem (cdr tree)))
        (cons elem tree)))))

(define (create-huffman string-list)
  (let ((total (apply + (map string-length string-list)))
        (occurences (get-occurences string-list)))
    (let loop ((tree (order occurences)))
      (if (equal? (length tree) 1)
        tree
        (let* ((e1 (car tree))
               (tree (cdr tree))
               (e2 (car tree))
               (tree (cdr tree)))
          (loop (add-in-order (cons (list e1 e2) (+ (cdr e1) (cdr e2))) tree)))))))

(define (huffman->code huffman)
  (match huffman
    ('() '())
    ((((h1 h2) . weight))
     (append
       (map (lambda (e) (cons (car e) (cons 0 (cdr e)))) (huffman->code h1))
       (map (lambda (e) (cons (car e) (cons 1 (cdr e)))) (huffman->code h2))))
    (((h1 h2) . weight)
     (append
       (map (lambda (e) (cons (car e) (cons 0 (cdr e)))) (huffman->code h1))
       (map (lambda (e) (cons (car e) (cons 1 (cdr e)))) (huffman->code h2))))
    ((((? char? char) . weight))
     (list (cons char '())))
    (((? char? char) . weight)
     (list (cons char '())))))

(define (bits->bytes b)
  (match b
   ('() '())
   ((a b c d e f g h bs ...)
    (cons (+ h (* 2 g) (* 4 f) (* 8 e) (* 16 d) (* 32 c) (* 64 b) (* 128 a))
      (bits->bytes bs)))
   (_ (bits->bytes (append b (make-list (- 8 (length b)) 0))))))

(define (huffman-encode code str)
  (u8-list->bytevector
    (bits->bytes
      (apply append (map (lambda (c) (or (assoc-ref code c) (throw "not in huffman code" c)))
                         (append (string->list str) (list #\nul)))))))

(define (byte->bits b)
  (let loop ((i 2) (b b) (by '()))
    (if (eq? i 512)
      by
      (loop (* i 2) (- b (modulo b i)) (cons (if (eq? (modulo b i) 0) 0 1) by)))))

(define (huffman-decode huffman bv)
  (let ((seq (apply append (map byte->bits (bytevector->u8-list bv)))))
    (list->string
      (reverse
        (let loop ((seq seq) (result '()) (huff huffman))
          (if (null? seq)
            result
            (match huff
              ((((h1 h2) . weight))
               (if (= (car seq) 0)
                 (loop (cdr seq) result h1)
                 (loop (cdr seq) result h2)))
              (((h1 h2) . weight)
               (if (= (car seq) 0)
                 (loop (cdr seq) result h1)
                 (loop (cdr seq) result h2)))
              ((((? char? char) . weight))
               (if (equal? char #\nul)
                 result
                 (loop seq (cons char result) huffman)))
              (((? char? char) . weight)
               (if (equal? char #\nul)
                 result
                 (loop seq (cons char result) huffman))))))))))

(define (serialize-huffman huffman)
  (define (serialize huffman)
    (match huffman
      ((((h1 h2) . weight))
       (append '(1) (serialize h1) (serialize h2)))
      (((h1 h2) . weight)
       (append '(1) (serialize h1) (serialize h2)))
      ((((? char? char) . weight))
       (append (bytevector->u8-list (string->utf8 (list->string (list char)))) '(0)))
      (((? char? char) . weight)
       (append (bytevector->u8-list (string->utf8 (list->string (list char)))) '(0)))))
  (u8-list->bytevector (serialize huffman)))

(define (serialize-huffman-string huffman-code)
  (lambda (str pos bv)
    (let ((sbv (huffman-encode huffman-code str)))
      (bytevector-copy! sbv 0 bv pos (bytevector-length sbv))
      (+ pos (bytevector-length sbv)))))

(define (huffman-string-size huffman-code)
  (lambda (str)
    (let ((sbv (huffman-encode huffman-code str)))
      (+ (bytevector-length sbv)))))
