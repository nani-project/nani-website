;;; Nani Project website
;;; Copyright © 2020 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of the Nani Project website.
;;;
;;; The Nani Project website is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; The Nani Project website is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with the Nani Project website.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nani encoding serialize)
  #:use-module (rnrs bytevectors)
  #:export (merge-bvs
            serialize-list list-size
            serialize-char char-size
            serialize-u16 u16-size
            serialize-int int-size
            serialize-boolean boolean-size
            serialize-string string-size))

;;; merge-bvs bvs
;;;   Returns a bytevector that contains the data from `bvs`, a list of bytevectors.  For instance,
;;;   (merge-bvs '(#u8(1 2) #u8(3 4))) gives #u8(1 2 3 4).
;;;
;;; Serializers and Sizers
;;; ----------------------
;;; 
;;; A serializer is a procedure that takes a value to serialize, a position at which to serialize
;;; the value, and a bytevector in which to serialize: `serialize val pos bv`.
;;;
;;; A sizer is a procedure that takes a value to serialize, and returns the expected size it will
;;; take in the serialization.
;;;
;;; Common Serialization Functions
;;; ------------------------------
;;;
;;; serialize-list serialize-element [#:size? #t]
;;;   Returns a serializer for a list of elements. `serialize-element` is the serializer function for
;;;   an element.  When `#:size` is `#t`, add the size of the list to the serialization, as a 16-bits
;;;   unsigned integer.
;;;
;;; list-size element-size [#:size? #t]
;;;   Returns a sizer for lists, where elements are sized by `element-size`.

(define (merge-bvs bvs)
  (let* ((size (apply + (map bytevector-length bvs)))
         (bv (make-bytevector size 0)))
    (let loop ((bvs bvs) (pos 0))
      (unless (null? bvs)
        (let ((sz (bytevector-length (car bvs))))
          (bytevector-copy! (car bvs) 0 bv pos sz)
          (loop (cdr bvs) (+ pos sz)))))
    bv))

(define* (serialize-list serialize-element #:key (size? #t))
  (lambda (lst pos bv)
    (when (not (list? lst)) (throw 'not-list lst))
    (when size?
      (bytevector-u16-set! bv pos (length lst) (endianness big)))
    (let loop ((lst lst) (pos (+ pos (if size? 2 0))))
      (if (null? lst)
        pos
        (loop (cdr lst) (serialize-element (car lst) pos bv))))))
(define* (list-size element-size #:key (size? #t))
  (lambda (lst)
    (when (not (list? lst)) (throw 'not-list lst))
    (apply + (if size? 2 0) (map element-size lst))))

(define (serialize-char int pos bv)
  (bytevector-u8-set! bv pos int)
  (+ pos 1))
(define char-size (const 1))

(define (serialize-u16 int pos bv)
  (bytevector-u16-set! bv pos int (endianness big))
  (+ pos 2))
(define u16-size (const 2))

(define (serialize-int int pos bv)
  (bytevector-u32-set! bv pos int (endianness big))
  (+ pos 4))
(define int-size (const 4))

(define (serialize-boolean bool pos bv)
  (bytevector-u8-set! bv pos (if bool 1 0))
  (+ pos 1))
(define boolean-size (const 1))

(define (serialize-string str pos bv)
  (let ((sbv (string->utf8 str)))
    (bytevector-copy! sbv 0 bv pos (bytevector-length sbv))
    (bytevector-u8-set! bv (+ pos (bytevector-length sbv)) 0)
    (+ pos 1 (bytevector-length sbv))))
(define (string-size str)
  (let ((sbv (string->utf8 str)))
    (+ 1 (bytevector-length sbv))))
