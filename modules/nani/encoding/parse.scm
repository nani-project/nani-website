;;; Nani Project website
;;; Copyright © 2020 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of the Nani Project website.
;;;
;;; The Nani Project website is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; The Nani Project website is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with the Nani Project website.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nani encoding parse)
  #:use-module (ice-9 binary-ports)
  #:use-module (rnrs bytevectors)
  #:export (parse-list
            parse-char
            parse-int
            parse-boolean
            parse-string))

(define* (parse-list parse-element #:key (size #f))
  (lambda (port)
    (let ((size (or size (bytevector-u16-ref (get-bytevector-n port 2) 0 (endianness big)))))
      (let loop ((result '()) (remaining size))
        (if (= remaining 0)
            (reverse result)
            (loop (cons (parse-element port) result) (- remaining 1)))))))

(define (parse-char port)
  (get-u8 port))

(define (parse-int port)
  (bytevector-u32-ref (get-bytevector-n port 4) 0 (endianness big)))

(define (parse-boolean port)
  (= (get-u8 port) 1))

(define (parse-string port)
  (define (get-result-string port)
    (let loop ((lu8 '()) (char (get-u8 port)))
      (if (= char 0)
          lu8
          (loop (cons char lu8) (get-u8 port)))))
  (utf8->string (u8-list->bytevector (reverse (get-result-string port)))))
