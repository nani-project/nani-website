;;; Nani Project website
;;; Copyright © 2020 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of the Nani Project website.
;;;
;;; The Nani Project website is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; The Nani Project website is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with the Nani Project website.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nani kanji radk)
  #:use-module (ice-9 binary-ports)
  #:use-module (ice-9 match)
  #:use-module (ice-9 peg)
  #:use-module (ice-9 rdelim)
  #:use-module (nani encoding parse)
  #:use-module (nani encoding serialize)
  #:use-module (rnrs bytevectors)
  #:use-module (sxml simple)
  #:export (%RADK-VERSION
            parse-radk
            get-kanji-stroke
            get-rad-kanji
            get-rad-stroke
            serialize-radk
            kanji-count))

(define %RADK-VERSION "NANI_RADK001")

;; PEG parser for the radk file
(define-peg-pattern comment none (and "#" (* (or "\t" (range #\x20 #\x10ffff))) "\n"))
(define-peg-pattern space none " ")
(define-peg-pattern return none "\n")
(define-peg-pattern entry all
  (and (ignore "$") space char space num (? (and space name)) (ignore "\n")
       (+ (or char (ignore "\n")))))
(define-peg-pattern num all (+ (or (range #\0 #\9))))
(define-peg-pattern name none (+ (or (range #\0 #\9) (range #\a #\z) (range #\A #\Z))))
(define-peg-pattern char all (and (range #\xff #\x10ffff)))
(define-peg-pattern radk-doc body (* (or return comment entry)))

;; parse the file with the peg parser, to low-level alist structure
(define (parse-radk file)
  (peg:tree (match-pattern radk-doc (call-with-input-file file read-string))))

(define (get-rad-kanji content)
  (let loop ((result '()) (content content))
    (match content
      (() result)
      ((('entry ('char radical) ('num stroke) ('char kanji)) content ...)
       (loop (cons (list radical kanji) result)
             content))
      ((('entry ('char radical) ('num stroke) (('char kanji) ...)) content ...)
       (loop (cons (cons radical kanji) result)
             content)))))

(define (get-rad-stroke content)
  (let loop ((result '()) (content content))
    (match content
      (() result)
      ((('entry ('char radical) ('num stroke) ('char kanji)) content ...)
       (loop (cons (cons radical (string->number stroke)) result)
             content))
      ((('entry ('char radical) ('num stroke) (('char kanji) ...)) content ...)
       (loop (cons (cons radical (string->number stroke)) result)
             content)))))

;; return an alist where keys are kanjis, and values the associated stroke count.
(define (get-kanji-stroke file)
  (define strokes (xml->sxml (call-with-input-file file read-string)))

  (match strokes
    (('*TOP* _ ('kanjidic2 content ...))
     (map
       (lambda (entry)
         (let* ((literal (car (assoc-ref entry 'literal)))
                (misc (assoc-ref entry 'misc))
                (misc (filter list? misc))
                (stroke (car (assoc-ref misc 'stroke_count))))
           (cons literal (string->number stroke))))
       (filter
         (lambda (entry)
           (and
             (list? entry)
             (equal? (car entry) 'character)))
         content)))))

(define (serialize-radk rad-kanji rad-stroke kanji-stroke)
  (define (serialize-rad-kanji-element element pos bv)
    (match element
      ((radical kanji ...)
       (let* ((pos (serialize-string (radical-character radical) pos bv)))
         (serialize-string (string-join kanji "") pos bv)))))
  (define (rad-kanji-element-size element)
    (match element
      ((radical kanji ...)
       (+ (string-size (radical-character radical))
          (string-size (string-join kanji ""))))))

  (define (serialize-rad-kanji rad-kanji pos bv)
    ((serialize-list serialize-rad-kanji-element) rad-kanji pos bv))
  (define (rad-kanji-size rad-kanji)
    ((list-size rad-kanji-element-size) rad-kanji))

  (define (serialize-rad-stroke-element element pos bv)
    (match element
      ((radical . stroke)
       (let ((pos (serialize-string (radical-character radical) pos bv)))
         (serialize-char stroke pos bv)))))
  (define (rad-stroke-element-size element)
    (match element
      ((radical . stroke)
       (+ (string-size (radical-character radical)) (char-size stroke)))))

  (define (serialize-rad-stroke rad-stroke pos bv)
    ((serialize-list serialize-rad-stroke-element) rad-stroke pos bv))
  (define (rad-stroke-size rad-stroke)
    ((list-size rad-stroke-element-size) rad-stroke))

  (define (serialize-kanji-stroke-element element pos bv)
    (match element
      ((kanji . stroke)
       (let ((pos (serialize-string kanji pos bv)))
         (serialize-char stroke pos bv)))))
  (define (kanji-stroke-element-size element)
    (match element
      ((kanji . stroke)
       (+ (string-size kanji) (char-size stroke)))))

  (define (serialize-kanji-stroke kanji-stroke pos bv)
    ((serialize-list serialize-kanji-stroke-element) kanji-stroke pos bv))
  (define (kanji-stroke-size kanji-stroke)
    ((list-size kanji-stroke-element-size) kanji-stroke))

  (let* ((header (string->utf8 %RADK-VERSION))
         (header-size (bytevector-length header))
         (bv (make-bytevector (+ header-size 12
                                 (rad-kanji-size rad-kanji)
                                 (rad-stroke-size rad-stroke)
                                 (kanji-stroke-size kanji-stroke)))))
    (bytevector-copy! header 0 bv 0 header-size)
    (let* ((pos header-size)
           (pos (serialize-rad-kanji rad-kanji pos bv))
           (pos (serialize-rad-stroke rad-stroke pos bv))
           (pos (serialize-kanji-stroke kanji-stroke pos bv)))
      bv)))

(define (radical-character kanji)
  (match kanji
    ("化" "⺅")
    ("个" "𠆢")
    ("并" "丷")
    ("刈" "⺉")
    ("乞" "𠂉")
    ("込" "⻌")
    ("尚" "⺌")
    ("忙" "⺖")
    ("扎" "⺘")
    ("汁" "⺡")
    ("犯" "⺨")
    ("艾" "⺾")
    ("邦" "⻏")
    ("阡" "⻖")
    ("老" "⺹")
    ("杰" "⺣")
    ("礼" "⺭")
    ("疔" "疒")
    ("禹" "禸")
    ("初" "⻂")
    ("買" "⺲")
    ("滴" "啇")
    (_ kanji)))

;; parse a serialized file back to the guile structure
(define (parse file)
  (define (parse-rad-kanji-element port)
    (let ((radical (parse-string port))
          (kanji-list (parse-string port)))
       (cons radical (string->list kanji-list))))
  (define (parse-rad-kanji port)
    ((parse-list parse-rad-kanji-element) port))

  (define (parse-rad-stroke-element port)
    (let ((radical (parse-string port))
          (stroke (parse-char port)))
       (cons radical stroke)))
  (define (parse-rad-stroke port)
    ((parse-list parse-rad-stroke-element) port))

  (define (parse-kanji-stroke-element port)
    (let ((kanji (parse-string port))
          (stroke (parse-char port)))
      (cons kanji stroke)))
  (define (parse-kanji-stroke port)
    ((parse-list parse-kanji-stroke-element) port))

  (call-with-input-file file
    (lambda (port)
      (let* ((header (utf8->string (get-bytevector-n port 12)))
             (rad-kanji (parse-rad-kanji port))
             (rad-stroke (parse-rad-stroke port))
             (kanji-stroke (parse-kanji-stroke port)))
        (list rad-kanji rad-stroke kanji-stroke)))))

(define (get-kanji-list content)
  (let loop ((result '()) (content content))
    (match content
      (() result)
      (((_ kanji ...) content ...)
       (loop (append result (filter (lambda (k) (not (member k result))) kanji))
             content)))))

(define (kanji-count file)
  (match (parse file)
    ((rad-kanji _ _)
     (length (get-kanji-list rad-kanji)))))
