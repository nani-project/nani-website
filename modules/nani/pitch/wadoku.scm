;;; Nani Project website
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of the Nani Project website.
;;;
;;; The Nani Project website is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; The Nani Project website is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with the Nani Project website.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nani pitch wadoku)
  #:use-module (ice-9 match)
  #:use-module (nani pitch pitch)
  #:use-module (srfi srfi-1)
  #:use-module (sxml ssax)
  #:export (xml->pitch))

(define (sxml->element lst elem)
  (let ((elem (match elem
                ((_ . elem) elem)
                (_ elem))))
    (match elem
      ('accent `(pitch . ,(car lst)))
      ('hira `(reading . ,(car lst)))
      ('orth (let ((kanji (filter string? lst)))
               (if (null? kanji) #f `(kanji . ,(car kanji)))))
      ('form
       `(form . ,(append-map (lambda (a) (if (list? a) a (list a))) lst)))
      ('reading (filter pair? lst))
      ('entry
        (let loop ((lst lst) (kanjis '()) (readings '()) (accents '()))
          (if (null? lst)
              (if (or (and (null? kanjis) (null? readings)) (null? accents))
                  #f
                  (append-map
                    (lambda (reading)
                      (map
                        (lambda (kanji)
                          (make-pitch kanji reading accents))
                        kanjis))
                    readings))
              (match (car lst)
                (('form . f)
                 (loop (append f (cdr lst)) kanjis readings accents))
                (('pitch . pitch)
                 (loop (cdr lst) kanjis readings (cons pitch accents)))
                (('reading . reading)
                 (loop (cdr lst) kanjis (cons reading readings) accents))
                (('kanji . kanji)
                 (loop (cdr lst) (cons kanji kanjis) readings accents))
                (_ (loop (cdr lst) kanjis readings accents))))))
      (_ #f))))

(define parser
  (ssax:make-parser
    NEW-LEVEL-SEED
    (lambda (elem-gi attributes namespaces expected-content seed)
      attributes)
    
    FINISH-ELEMENT
    (lambda (elem-gi attributes namespaces parent-seed seed)
      (if (equal? elem-gi 'entries)
          seed
          (let* ((seed (reverse seed))
                 (element (sxml->element seed elem-gi)))
            (cons element parent-seed))))
    
    CHAR-DATA-HANDLER
    (lambda (string1 string2 seed)
      (cons (string-append string1 string2) seed))))

(define (xml->pitch port)
  (filter pitch? (apply append (filter list? (parser port '())))))
