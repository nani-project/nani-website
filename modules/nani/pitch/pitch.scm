;;; Nani Project website
;;; Copyright © 2021 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of the Nani Project website.
;;;
;;; The Nani Project website is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; The Nani Project website is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with the Nani Project website.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nani pitch pitch)
  #:use-module (ice-9 binary-ports)
  #:use-module (ice-9 match)
  #:use-module (nani encoding huffman)
  #:use-module (nani encoding trie)
  #:use-module (rnrs bytevectors)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:export (%PITCH-VERSION
            make-pitch
            pitch?
            pitch-kanji
            pitch-reading
            pitch-accents

            serialize-pitch
            pitch-entry-count))

(define %PITCH-VERSION "NANI_PITCH001")

(define-record-type pitch
  (make-pitch kanji reading accents)
  pitch?
  (kanji   pitch-kanji)
  (reading pitch-reading)
  (accents pitch-accents))

(define (make-trie-key key)
  (append-map
    (lambda (c)
      (list (quotient c 16) (modulo c 16)))
    (bytevector->u8-list (string->utf8 key))))

(define (get-pitch-trie pitches)
  (let ((trie (make-empty-trie)))
    (for-each
      (lambda (pitch)
        (let ((key (string-append (pitch-kanji pitch) (pitch-reading pitch))))
          (for-each
            (lambda (accent)
              (add-to-trie! trie (make-trie-key key) accent))
            (pitch-accents pitch))))
      pitches)
    (compress-trie trie)))

(define (uniq lst)
  (let loop ((lst lst) (res '()))
    (match lst
      (() res)
      ((elem lst ...)
       (if (member elem res)
           (loop lst res)
           (loop lst (cons elem res)))))))

(define (collapse-vals! trie)
  (let ((transitions (trie-transitions trie))
        (vals (map (match-lambda ((? string? s) s) ((? number? i) (number->string i)))
                   (uniq (trie-vals trie)))))
    (trie-vals-set! trie (string-join vals ", "))
    (for-each collapse-vals! (map cdr transitions))))

(define (collect-vals trie)
  (let ((transitions (trie-transitions trie))
        (vals (trie-vals trie)))
    (cons vals (append-map collect-vals (map cdr transitions)))))

(define (serialize-pitch pitches)
  (let ((trie (get-pitch-trie pitches)))
    (collapse-vals! trie)
    (let* ((huffman (create-huffman (collect-vals trie)))
           (code (huffman->code huffman)))
      (let* ((header (string->utf8 %PITCH-VERSION))
             (header-size (bytevector-length header))
             (huffman-bv (serialize-huffman huffman))
             (huffman-size (bytevector-length huffman-bv))
             (trie-size ((trie-size-single (huffman-string-size code)) trie))
             (result (make-bytevector (+ header-size 4 huffman-size trie-size))))
        (bytevector-copy! header 0 result 0 header-size)
        (bytevector-u32-set! result header-size (length pitches) (endianness big))
        (bytevector-copy! huffman-bv 0 result (+ header-size 4) huffman-size)
        ((serialize-trie-single (serialize-huffman-string code)
                                (huffman-string-size code))
         trie (+ header-size 4 huffman-size) result)
        result))))

(define (pitch-entry-count file)
  (call-with-input-file file
    (lambda (port)
      ;; header
      (get-bytevector-n port 13)
      ;; size
      (bytevector-u32-ref (get-bytevector-n port 4) 0 (endianness big)))))
