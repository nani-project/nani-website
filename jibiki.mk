DICOS+=dicos/jibiki_fre.nani
DOWNLOADS+=dictionaries/jibiki.xml

dictionaries/jibiki.xml:
	wget https://jibiki.fr/data/Jibiki.fr/jibiki.fr_jpn_fra.xml.gz -O $@.gz
	gunzip $@.gz

dicos/jibiki_fre.nani: dictionaries/jibiki.xml tools/jibiki.scm dictionaries/frequency.tsv $(DICO_MODULES)
	guile -L modules tools/jibiki.scm build $< $@
