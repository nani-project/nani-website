KANJIDIC_LANGS=en es fr pt
KANJIDIC_MODULES=tools/kanjidic.scm
DICOS+=$(addprefix dicos/kanjidic_, $(addsuffix .nani, $(KANJIDIC_LANGS)))
DOWNLOADS+=dictionaries/kanjidic2.xml

dictionaries/kanjidic2.xml:
	wget http://www.edrdg.org/kanjidic/kanjidic2.xml.gz -O $@.gz
	gunzip $@.gz

dicos/kanjidic_%.nani: dictionaries/kanjidic2.xml tools/kanjidic.scm $(RADK_MODULES)
	guile -L modules tools/kanjidic.scm build $< $(shell basename $@ .nani | sed 's|^kanjidic_||g') $@
