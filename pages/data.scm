;;; Nani Project website
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of the Nani Project website.
;;;
;;; The Nani Project website is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; The Nani Project website is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with the Nani Project website.  If not, see <http://www.gnu.org/licenses/>.

(define-module (pages data)
  #:use-module (tools haunt-i18n)
  #:use-module (tools i18n)
  #:use-module (tools theme)
  #:export (page-data))

(define page-data
  (internationalize "data" "data"
    `(div
      (h1 (_ "Data Sources"))
      (div (@ (class "article"))
        (p (_ "Nani uses various open data sources to provide its functionalities.
This page lists data sources, content and processes."))
        (h2 (_ "Electronic Dictionary Research and Development Group"))
        (p (_ "The Electronic Dictionary Research and Development Group was
established in 2000 within the Faculty of Information Technology, Monash
University. Its goals are providing electronic dictionaries and
computational linguistics."))
        (h3 (_ "JMdict"))
        (p (_ "The <a href=\"~a\">JMdict</a> project has as its goal the
production of a freely available Japanese/English Dictionary in
machine-readable form."
               "https://www.edrdg.org/jmdict/edict_doc.html"))
        (p (_ "This source is licensed under <a href=\"~a\">Creative Commons
Share-Alike</a>. This data is available in monolingual (English) and multilingual,
including Dutch, French, German and others."
               "https://creativecommons.org/licenses/by-sa/3.0/"))
        (p (_ "The original files are XML files with lots of entries. Since it's
hard to search inside such big XML files, we modify them into a binary format
that contains a search tree which makes it so much efficient to look for data."))))
    nani-theme))
