;;; Nani Project website
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of the Nani Project website.
;;;
;;; The Nani Project website is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; The Nani Project website is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with the Nani Project website.  If not, see <http://www.gnu.org/licenses/>.

(define-module (pages blog)
  #:use-module (tools haunt-i18n)
  #:use-module (tools i18n)
  #:use-module (tools theme)
  #:use-module (haunt builder blog)
  #:use-module (haunt post)
  #:export (blog-pages get-posts))

(define (get-posts posts)
  "Returns POSTS sorted in reverse chronological order, excluding drafts."
  (filter (lambda (post)
            (not (member "draft" (post-ref post 'tags))))
    (posts/reverse-chronological posts)))

(define (get-lang-posts lang)
  (lambda (posts)
    "Returns POSTS sorted in reverse chronological order, excluding drafts."
    (filter (lambda (post)
              (and (not (member "draft" (post-ref post 'tags)))
                   (member lang (post-ref post 'tags))))
      (posts/reverse-chronological posts))))

(define (get-every-posts posts)
  (filter (lambda (post)
            (not (member "draft" (post-ref post 'tags))))
    (posts/reverse-chronological posts)))

(define get-posts-en (get-lang-posts "en"))

(define nani-articles-en
`(("Articles" "blog.en.html" ,get-posts-en)))


(define %articles
`(("Articles" "blog.html" ,get-posts)))

(define %complete-articles
`(("Articles" "blog-complete.html" ,get-posts)))
(define %complete-articles-en
`(("Articles" "blog-complete.en.html" ,get-posts))) 

(define blog-pages
  (list 
    (blog #:theme (nani-theme "en" "blog") #:collections nani-articles-en)
    (blog #:theme (nani-theme "en" "blog-complete") #:collections %complete-articles-en)))
