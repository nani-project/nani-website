;;; Nani Project website
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of the Nani Project website.
;;;
;;; The Nani Project website is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; The Nani Project website is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with the Nani Project website.  If not, see <http://www.gnu.org/licenses/>.

(define-module (pages index)
  #:use-module (tools haunt-i18n)
  #:use-module (tools i18n)
  #:use-module (tools theme)
  #:export (page-index))

(define page-index
  (internationalize "index" "index"
    `(div
       (div (@ (id "hero"))
         (h1 (_ "OFFLINE JAPANESE DICTIONARY"))
         (nav
           (a (@ (href "https://f-droid.org/app/eu.lepiller.nani"))
              ;; Get more badge urls at
              ;; https://gitlab.com/fdroid/artwork/tree/master/badge
              ;; TRANSLATORS: rename this file to /images/get-it-on-<lang>.png
              (_ "<img src=\"/images/get-it-on-en.png\" />"))))
       (h2 (@ (id "after-hero")) (_ "Features"))
       (p (_ "Offline Japanese dictionary, with meaning, pronunciation, kanji and more!"))
       (div (@ (id "features"))
        (div
          (h3 (_ "Specialized dictionary"))
          (ul
            (li (_ "Search by kanji, reading and meaning"))
            (li (_ "Multiple propositions"))
            (li (_ "Works offline with the features you want"))
            (li (_ "Multiple languages supported: English, French, German, and more"))))
        (div
          (h3 (_ "Free Software and Open Data"))
          (ul
            (li (_ "Free software application"))
            (li (_ "No tracker, no ad, no suspicious permission"))
            (li (_ "Many open <a href=\"/data.html\">data providers</a>"))
            (li (_ "You can build it, contribute to it and its data sources"))))))
    nani-theme))
