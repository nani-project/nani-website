;;; Nani Project website
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of the Nani Project website.
;;;
;;; The Nani Project website is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; The Nani Project website is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with the Nani Project website.  If not, see <http://www.gnu.org/licenses/>.

(define-module (pages documentation)
  #:use-module (tools haunt-i18n)
  #:use-module (tools i18n)
  #:use-module (tools theme)
  #:export (page-documentation))

(define page-documentation
  (internationalize "documentation" "documentation"
    `(div
      (h1 (_ "Documentation"))
      (div (@ (class "article"))
        (p (_ "This documentation will guide you in some of the most important
aspects of the application. Please read it carefully!"))
        (h2 (_ "Downloading a dictionary"))
        (p (_ "This application is based on multiple dictionaries that
you can download in the app. They all add some functionality to the application.
In the following sections we will see how to use them."))
        (p (_ "To download a dictionary, you need to click on the three dots
at the top right of the screens and select “Manage Dictionaries”. This will
open a new view where you can select a dictionary in a list."))
        (p (_ "Tap on the dictionary you want to add. You will be presented
with more details on the dictionary and options to download, refresh or delete
it. Once you have successfully downloaded a dictionary, you are ready to use
the app!"))
        (h2 (_ "Searching vocabulary"))
        (p (_ "To be able to look for vocabulary, you must first download a
dictionary. There are dictionaries for different languages."))
        (p (_ "From the main view, tap on the search bar and type your search
term. It can be a word writen in kanji, with its pronunciation or the meaning
of a word in the language of a dictionary you downloaded."))
        (p (_ "Tap on the search button and you'll see the results. Easy, right?"))
        (h2 (_ "Settings"))
        (p (_ "There are currently no settings."))
        (h2 (_ "Building the app"))
        (p (_ "To develop the app, I use android studio. This is the best way to
build it, because you can easily modify some of it too. If you don't want to
use android studio, you can still build the app with only gradle."))
        (h2 (_ "You can help too!"))
        (p (_ "Lastly, note that you can help make this app the best. You can help
in so many different ways, some of the greatest are listed below:"))
        (ul
          (li (_ "<a href=\"~a\">Translate</a> the app and this website"
                  "https://framagit.org/nani-project"))
          (li (_ "Talk about it to fellow Japanese learners"))
          (li (_ "<a href=\"~a\">Report</a> a bug, or two"
                  "https://framagit.org/nani-project/nani-app/issues"))
          (li (_ "<a href=\"~a\">Suggest</a> improvements and more data sources"
                  "https://framagit.org/nani-project/nani-app/issues"))
          (li (_ "Write some code and send me a patch")))
        (p (_ "In any case, I hope you will enjoy Nani as much as I enjoyed writing
it!"))))
    nani-theme))
