;;; Nani Project website
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of the Nani Project website.
;;;
;;; The Nani Project website is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; The Nani Project website is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with the Nani Project website.  If not, see <http://www.gnu.org/licenses/>.

(define-module (pages mentions)
  #:use-module (tools haunt-i18n)
  #:use-module (tools i18n)
  #:use-module (tools theme)
  #:export (page-mentions))

(define page-mentions
  (internationalize "Mentions légales" "mentions"
    `(article (@ (class "article"))
       (h1 (_ "Legal notices"))
       (p (_ "This website is edited by Julien Lepiller and hosted by <a href=\"~a\" target=\"_blank\">One provider</a>."
             "https://oneprovider.com"))
       (p (_ "Head office: ") "3275 Av Francis-Hughes, Laval, QC H7L 5A5 Canada")
       (p (_ "Phone: ") "+1.514.286.0253")
       (p (_ "This website and the application do not collect or store personal information
other than what is technically necessary to deliver web pages and content to the user.")))
    nani-theme))
