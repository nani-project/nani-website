# French translations for PACKAGE package.
# Copyright (C) 2019 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# root <julien@lepiller.eu>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-03-10 11:26+0100\n"
"PO-Revision-Date: 2024-03-10 10:21+0000\n"
"Last-Translator: Julien Lepiller <fedora-account@lepiller.eu>\n"
"Language-Team: French <https://translate.fedoraproject.org/projects/nani/"
"website/fr/>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.4\n"

#: pages/documentation.scm:63
#, scheme-format
msgid "<a href=\"~a\">Report</a> a bug, or two"
msgstr "<a href=\"~a\">Rapportez</a> un bogue, voir deux,"

#: pages/documentation.scm:65
#, scheme-format
msgid "<a href=\"~a\">Suggest</a> improvements and more data sources"
msgstr ""
"<a href=\"~a\">Suggérez</a> des améliorations et des sources de données "
"supplémentaires,"

#: pages/documentation.scm:60
#, scheme-format
msgid "<a href=\"~a\">Translate</a> the app and this website"
msgstr "<a href=\"~a\">Traduisez</a> l'appli et ce site web"

#. Get more badge urls at
#. https://gitlab.com/fdroid/artwork/tree/master/badge
#. TRANSLATORS: rename this file to /images/get-it-on-<lang>.png
#: pages/index.scm:35
msgid "<img src=\"/images/get-it-on-en.png\" />"
msgstr "<img src=\"/images/get-it-on-fr.png\" />"

#: tools/theme.scm:57
msgid "Build it"
msgstr "Construisez-la"

#: pages/documentation.scm:52
msgid "Building the app"
msgstr "Construire l'application"

#: pages/data.scm:28
msgid "Data Sources"
msgstr "Sources de données"

#: pages/documentation.scm:28 tools/theme.scm:55
msgid "Documentation"
msgstr "Documentation"

#: pages/documentation.scm:32
msgid "Downloading a dictionary"
msgstr "Télécharger un dictionnaire"

#: pages/data.scm:32
msgid "Electronic Dictionary Research and Development Group"
msgstr "Electronic Dictionary Research and Development Group"

#. TRANSLATORS: translate this with the name of your language.
#: tools/theme.scm:70
msgid "English"
msgstr "français"

#: pages/index.scm:36 tools/theme.scm:52
msgid "Features"
msgstr "Fonctionnalités"

#: pages/index.scm:47
msgid "Free Software and Open Data"
msgstr "Logiciel libre et open data"

#: pages/index.scm:49
msgid "Free software application"
msgstr "Cette application est un logiciel libre"

#: pages/documentation.scm:46
msgid ""
"From the main view, tap on the search bar and type your search\n"
"term. It can be a word writen in kanji, with its pronunciation or the "
"meaning\n"
"of a word in the language of a dictionary you downloaded."
msgstr ""
"Depuis la vue principale, appuyez sur la barre de recherche et saisissez "
"votre recherche. Elle peut contenir un mot écrit en kanji, sa prononciation "
"en hiragana ou son sens dans l'une des langues pour lesquelles vous avez "
"téléchargé un dictionnaire."

#: pages/mentions.scm:31
msgid "Head office: "
msgstr "Siège social : "

#: pages/documentation.scm:68
msgid ""
"In any case, I hope you will enjoy Nani as much as I enjoyed writing\n"
"it!"
msgstr ""
"Dans tous les cas, j'espère que vous apprécierez Nani autant que j'ai "
"apprécié l'écrire !"

#: pages/data.scm:37
msgid "JMdict"
msgstr "JMdict"

#: tools/list.scm:91
msgid ""
"Japanese/Dutch dictionary from the Electronic Dictionary Research and "
"Development Group."
msgstr ""
"Dictionnaire japonais/néerlandais de l’Electronic Dictionary Research and "
"Development Group."

#: tools/list.scm:90
msgid ""
"Japanese/English dictionary from the Electronic Dictionary Research and "
"Development Group."
msgstr ""
"Dictionnaire japonais/anglais de l’Electronic Dictionary Research and "
"Development Group."

#: tools/list.scm:78
msgid "Japanese/French aligned sentences from the Tatoeba project."
msgstr "Phrases bilingues japonais/français alignées du projet Tatoeba."

#: tools/list.scm:92
msgid ""
"Japanese/French dictionary from the Electronic Dictionary Research and "
"Development Group."
msgstr ""
"Dictionnaire japonais/français de l’Electronic Dictionary Research and "
"Development Group."

#: tools/list.scm:62
msgid "Japanese/French dictionary from the Jibiki project."
msgstr "Dictionnaire japonais/français du projet Jibiki."

#: tools/list.scm:46
msgid "Japanese/German dictionary from Wadoku."
msgstr "Dictionnaire japonais/allemand de Wadoku."

#: tools/list.scm:93
msgid ""
"Japanese/German dictionary from the Electronic Dictionary Research and "
"Development Group."
msgstr ""
"Dictionnaire japonais/allemand de l’Electronic Dictionary Research and "
"Development Group."

#: tools/list.scm:94
msgid ""
"Japanese/Hungarian dictionary from the Electronic Dictionary Research and "
"Development Group."
msgstr ""
"Dictionnaire japonais/hongrois de l’Electronic Dictionary Research and "
"Development Group."

#: tools/list.scm:79
msgid "Japanese/Russian aligned sentences from the Tatoeba project."
msgstr "Phrases bilingues japonais/russe alignées du projet Tatoeba."

#: tools/list.scm:95
msgid ""
"Japanese/Russian dictionary from the Electronic Dictionary Research and "
"Development Group."
msgstr ""
"Dictionnaire japonais/russe de l’Electronic Dictionary Research and "
"Development Group."

#: tools/list.scm:96
msgid ""
"Japanese/Slovenian dictionary from the Electronic Dictionary Research and "
"Development Group."
msgstr ""
"Dictionnaire japonais/slovène de l’Electronic Dictionary Research and "
"Development Group."

#: tools/list.scm:80
msgid "Japanese/Spanish aligned sentences from the Tatoeba project."
msgstr "Phrases bilingues japonais/espagnol alignées du projet Tatoeba."

#: tools/list.scm:97
msgid ""
"Japanese/Spanish dictionary from the Electronic Dictionary Research and "
"Development Group."
msgstr ""
"Dictionnaire japonais/espagnol de l’Electronic Dictionary Research and "
"Development Group."

#: tools/list.scm:98
msgid ""
"Japanese/Swedish dictionary from the Electronic Dictionary Research and "
"Development Group."
msgstr ""
"Dictionnaire japonais/suédois de l’Electronic Dictionary Research and "
"Development Group."

#: tools/list.scm:81
msgid "Japanese/Ukrainian aligned sentences from the Tatoeba project."
msgstr "Phrases bilingues japonais/ukrainien alignées du projet Tatoeba."

#: tools/list.scm:77
msgid "Japanese/english aligned sentences from the Tatoeba project."
msgstr "Phrases bilingues japonais/anglais alignées du projet Tatoeba."

#: tools/list.scm:107
msgid "Kanji dictionary with English meanings."
msgstr "Dictionnaire de kanji avec leur signification en anglais."

#: tools/list.scm:109
msgid "Kanji dictionary with French meanings."
msgstr "Dictionnaire de kanji avec leur signification en français."

#: tools/list.scm:110
msgid "Kanji dictionary with Portuguese meanings."
msgstr "Dictionnaire de kanji avec leur signification en portugais."

#: tools/list.scm:108
msgid "Kanji dictionary with Spanish meanings."
msgstr "Dictionnaire de kanji avec leur signification en espagnol."

#: tools/list.scm:40
msgid "Kanji writing visual help by the Kanjivg project."
msgstr "Aide visuelle à l'écriture des kanji du projet Kanjivg."

#: tools/theme.scm:59
msgid "Language"
msgstr "Langue"

#: pages/documentation.scm:57
msgid ""
"Lastly, note that you can help make this app the best. You can help\n"
"in so many different ways, some of the greatest are listed below:"
msgstr ""
"Enfin, remarquez que vous pouvez aider à faire de cette appli une "
"référence ! Vous pouvez aider de nombreuses manières, dont voici quelques-"
"unes :"

#: pages/mentions.scm:28 tools/theme.scm:83
msgid "Legal notices"
msgstr "Mentions légales"

#: pages/index.scm:51
msgid "Many open <a href=\"/data.html\">data providers</a>"
msgstr "De nombreux <a href=\"/data.html\">fournisseurs open data</a>,"

#: pages/index.scm:45
msgid "Multiple languages supported: English, French, German, and more"
msgstr ""
"Plusieurs langues supportées : l'anglais, le français, l'allemand et bien "
"d'autres,"

#: pages/index.scm:43
msgid "Multiple propositions"
msgstr "Plusieurs propositions"

#: pages/data.scm:30
msgid ""
"Nani uses various open data sources to provide its functionalities.\n"
"This page lists data sources, content and processes."
msgstr ""
"Nani utilise diverses sources de données en open data pour fournir toutes "
"ses fonctionnalités. Cette page liste les sources de données, leur contenu "
"et la manière dont elles sont traitées."

#: pages/index.scm:50
msgid "No tracker, no ad, no suspicious permission"
msgstr "Pas de pisteur, de pub ni de permission suspecte,"

#: pages/index.scm:29
msgid "OFFLINE JAPANESE DICTIONARY"
msgstr "DICTIONNAIRE JAPONAIS HORS-LIGNE"

#: pages/index.scm:37
msgid ""
"Offline Japanese dictionary, with meaning, pronunciation, kanji and more!"
msgstr ""
"Dictionnaire japonais hors-ligne, avec la signification, la prononciation, "
"les kanji et bien plus encore !"

#: pages/mentions.scm:32
msgid "Phone: "
msgstr "Téléphone : "

#: tools/list.scm:54
msgid "Pitch accent dictionary from Wadoku."
msgstr "Dictionnaire d'accent de hauteur de Wadoku."

#: tools/list.scm:70
msgid ""
"Proper name dictionary from the Electronic Dictionary Research and "
"Development Group."
msgstr ""
"Dictionnaire de noms propres de l’Electronic Dictionary Research and "
"Development Group."

#: tools/list.scm:33
msgid ""
"Radical to Kanji dictionary from the Electronic Dictionary Research and "
"Development Group."
msgstr ""
"Dictionnaire de Kanji à partir des racines de l’Electronic Dictionary "
"Research and Development Group."

#: tools/theme.scm:115
msgid "Read"
msgstr "Lire"

#: pages/index.scm:42
msgid "Search by kanji, reading and meaning"
msgstr "Recherchez par kanji, lecture ou signification"

#: pages/documentation.scm:43
msgid "Searching vocabulary"
msgstr "Rechercher du vocabulaire"

#: pages/documentation.scm:50
msgid "Settings"
msgstr "Paramètres"

#: pages/index.scm:40
msgid "Specialized dictionary"
msgstr "Dictionnaire spécialisé"

#: pages/documentation.scm:62
msgid "Talk about it to fellow Japanese learners"
msgstr "Parlez-en autour de vous"

#: pages/documentation.scm:39
msgid ""
"Tap on the dictionary you want to add. You will be presented\n"
"with more details on the dictionary and options to download, refresh or "
"delete\n"
"it. Once you have successfully downloaded a dictionary, you are ready to "
"use\n"
"the app!"
msgstr ""
"Appuyez sur le dictionnaire que vous voulez ajouter. Vous arriverez sur une "
"page avec plus de détails sur le dictionnaire et des options pour "
"télécharger, mettre à jour et supprimer le dictionnaire. Une fois que vous "
"avez réussi à télécharger un dictionnaire, vous êtes prêt à utiliser "
"l'appli !"

#: pages/documentation.scm:49
msgid "Tap on the search button and you'll see the results. Easy, right?"
msgstr ""
"Appuyez sur le bouton de recherche et vous verrez les résultats. Plutôt "
"simple, non ?"

#: tools/list.scm:83
msgid ""
"Tatoeba is a collection of sentences and translations. This\n"
"        dictionary contains pairs of sentences that are direct translations "
"of\n"
"        one another, which allows you to see example sentences in search\n"
"        results."
msgstr ""
"Tatoeba est une collection de phrases et de traductions. Ce dictionnaire "
"contient des pairs de phrases qui sont une traduction directe l'une de "
"l'autre, ce qui vous permet de voir des exemples de phrases dans les "
"résultats de la recherche."

#: pages/e404.scm:27
msgid "That's a 404 :/"
msgstr "C'est une erreur 404 :/"

#: pages/data.scm:38
#, scheme-format
msgid ""
"The <a href=\"~a\">JMdict</a> project has as its goal the\n"
"production of a freely available Japanese/English Dictionary in\n"
"machine-readable form."
msgstr ""
"Le projet <a href=\"~a\">JMdict</a> a pour but de produire un dictionnaire "
"japonais/anglais librement disponible dans un format lisible par une machine."

#: pages/data.scm:33
msgid ""
"The Electronic Dictionary Research and Development Group was\n"
"established in 2000 within the Faculty of Information Technology, Monash\n"
"University. Its goals are providing electronic dictionaries and\n"
"computational linguistics."
msgstr ""
"Le <em lang=\"en\">Electronic Dictionary Research and Development Group</em> "
"a démarré en 2000 dans la faculté des technologies de l'information de "
"l'université de Monash. Son but est de fournir des dictionnaires "
"électroniques et la linguistique informatique."

#: pages/data.scm:46
msgid ""
"The original files are XML files with lots of entries. Since it's\n"
"hard to search inside such big XML files, we modify them into a binary "
"format\n"
"that contains a search tree which makes it so much efficient to look for "
"data."
msgstr ""
"Les fichiers de départ sont des fichiers XML avec beaucoup d'entrées. Comme "
"il est difficile de rechercher dans des XML aussi gros, nous les modifions "
"en un format binaire qui contient des arbres de recherche qui rendent plus "
"efficaces la recherche de données."

#: tools/theme.scm:84
#, scheme-format
msgid ""
"The source of this website can be seen at\n"
"<a href=\"~a\">framagit</a>. This website is free software; you can "
"redistribute\n"
"it and/or modify it under the terms of the GNU Affero General Public "
"License\n"
"as published by the Free Software Foundation; either version 3 of the "
"License,\n"
"or (at your option) any later version."
msgstr ""
"Vous pouvez voir la source de ce site sur <a href=\"~a\">framagit</a>. Ce "
"site est un logiciel libre ; vous pouvez le redistribuer ou le modifier sous "
"les termes de la licence GNU Affero General Public License telle que publiée "
"par la Free Software Foundation ; soit en version 3, soit (à votre gré) en "
"une version ultérieure."

#: pages/documentation.scm:51
msgid "There are currently no settings."
msgstr "Il n'y a pas encore de paramètre."

#: pages/documentation.scm:33
msgid ""
"This application is based on multiple dictionaries that\n"
"you can download in the app. They all add some functionality to the "
"application.\n"
"In the following sections we will see how to use them."
msgstr ""
"Cette application est basée sur divers dictionnaires que vous pouvez "
"télécharger dans l'appli. Ils ajoutent tous certaines fonctionnalités à "
"l'application. Dans les sections suivantes, nous verrons comment les "
"utiliser."

#: tools/list.scm:56
msgid ""
"This dictionary allows you to augment search results on the main view\n"
"         with pitch accent (pronunciation) information.  Japanese is not "
"flat,\n"
"         and this dictionary will add information that will help you "
"pronounce\n"
"         words better, with a standard Japanese pitch accent."
msgstr ""
"Ce dictionnaire vous permet d'améliorer les résultats de recherche de la\n"
"    vue principale avec des informations sur l'accent de hauteur (la\n"
"    prononciation).  Le japonais n'est pas plat, et ce dictionnaire vous\n"
"    aidera à mieux prononcer les mots, avec l'accent de hauteur du japonais\n"
"    standard."

#: tools/list.scm:64
msgid ""
"This dictionary allows you to do searches on the main view of this app.\n"
"\tFailing to download one of these dictionaries will make the app unusable\n"
"\tas you can't search for anything.  This dictionary can be searched for\n"
"\tby kanji, reading (kana) and by French translation."
msgstr ""
"Ce dictionnaire vous permet d’effectuer des recherches sur la vue\n"
"    principale de cette appli. Si vous n’en téléchargez aucun, l’appli\n"
"    sera inutilisable puisque vous ne pourrez rien rechercher. Ce\n"
"    dictionnaire permet d’effectuer des recherches par kanji, par\n"
"    prononciation (kana) et par traduction française."

#: tools/list.scm:48
msgid ""
"This dictionary allows you to do searches on the main view of this app.\n"
"        Failing to download one of these dictionaries will make the app "
"unusable\n"
"        as you can't search for anything.  This dictionary can be searched "
"for\n"
"        by kanji, reading (kana) and by German translation."
msgstr ""
"Ce dictionnaire vous permet d’effectuer des recherches sur la vue\n"
"    principale de cette appli. Si vous n’en téléchargez aucun, l’appli\n"
"    sera inutilisable puisque vous ne pourrez rien rechercher. Ce\n"
"    dictionnaire permet d’effectuer des recherches par kanji, par\n"
"    prononciation (kana) et par traduction allemande."

#: tools/list.scm:100
msgid ""
"This dictionary allows you to do searches on the main view of this app.\n"
"        Failing to download one of these dictionaries will make the app "
"unusable\n"
"        as you can't search for anything.  This dictionary can be searched "
"for by\n"
"        kanji, reading (kana) and by meaning in the languages you selected."
msgstr ""
"Ce dictionnaire vous permet d’effectuer des recherches sur la vue\n"
"    principale de cette appli. Si vous n’en téléchargez aucun, l’appli\n"
"    sera inutilisable puisque vous ne pourrez rien rechercher. Ce\n"
"    dictionnaire permet d’effectuer des recherches par kanji, par\n"
"    prononciation (kana) et par signification dans les langues que vous\n"
"    aurez téléchargées."

#: tools/list.scm:35
msgid ""
"This dictionary allows you to enter kanji by selecting some of its\n"
"    components.  Tap the water component button on the bottom of the screen "
"to\n"
"    access the kanji selection by component view"
msgstr ""
"Ce dictionnaire vous permet de saisir des kanji en choisissant\n"
"    certains de ses composants. Appuyez sur le bouton représentant la "
"racine\n"
"    de l'eau en bas de l'écran principal pour accéder à la vue de sélection\n"
"    des kanji à partir des composants"

#: tools/list.scm:72
msgid ""
"This dictionary allows you to extend searches on the main view of this\n"
"    app to proper names of people, companies, etc."
msgstr ""
"Ce dictionnaire vous permet d'étendre la recherche principale de cette "
"application\n"
"    au noms de personnes, d'entreprises, etc."

#: tools/list.scm:112
msgid ""
"This dictionary allows you to search for kanji and view kanji information\n"
"        such as number of strokes, pronunciations and meanings."
msgstr ""
"Ce dictionnaire vous permet de rechercher des kanji et d'obtenir des "
"informations comme le nombre de traits, la prononciation et la signification."

#: tools/list.scm:42
msgid ""
"This dictionary allows you to see how a kanji is written, what it is\n"
"composed of, and the order in which strokes are written."
msgstr ""
"Ce dictionnaire vous permet de voir comment un kanji est écrit, ce dont il "
"est composé, et l'ordre dans lequel les traits sont dessinés."

#: pages/documentation.scm:30
msgid ""
"This documentation will guide you in some of the most important\n"
"aspects of the application. Please read it carefully!"
msgstr ""
"Cette documentation vous guidera sur certains aspects les plus importants de "
"l'application. Lisez-la attentivement !"

#: pages/data.scm:42
#, scheme-format
msgid ""
"This source is licensed under <a href=\"~a\">Creative Commons\n"
"Share-Alike</a>. This data is available in monolingual (English) and "
"multilingual,\n"
"including Dutch, French, German and others."
msgstr ""
"Cette source est disponible sous license <a href=\"~a\">Creative Commons "
"Share-Alike</a>. Ces données sont disponibles sous forme monolingue "
"(anglais) et multilingue, dont le néerlandais, le français, l'allemand et "
"d'autres langues."

#: pages/mentions.scm:33
msgid ""
"This website and the application do not collect or store personal "
"information\n"
"other than what is technically necessary to deliver web pages and content to "
"the user."
msgstr ""
"Ce site web et l'application ne collectent ni ne stockent de données "
"personnelles autres que celles techniquement nécessaires pour délivrer les "
"pages web et le contenu à l'utilisateur."

#: pages/mentions.scm:29
#, scheme-format
msgid ""
"This website is edited by Julien Lepiller and hosted by <a href=\"~a\" "
"target=\"_blank\">One provider</a>."
msgstr ""
"Ce site web est édité par Julien Lepiller et hébergé par <a href=\"~a\" "
"target=\"_blank\">One provider</a>."

#: pages/documentation.scm:44
msgid ""
"To be able to look for vocabulary, you must first download a\n"
"dictionary. There are dictionaries for different languages."
msgstr ""
"Pour rechercher du vocabulaire, vous devez d'abord télécharger un "
"dictionnaire. Il y a des dictionnaires pour différentes langues."

#: pages/documentation.scm:53
msgid ""
"To develop the app, I use android studio. This is the best way to\n"
"build it, because you can easily modify some of it too. If you don't want "
"to\n"
"use android studio, you can still build the app with only gradle."
msgstr ""
"Pour développer l'appli, j'utilise Android Studio. C'est la meilleure façon "
"de la construire, parce que vous pouvez facilement la modifier. Si vous ne "
"voulez pas utiliser Android Studio, vous pouvez toujours construire "
"l'application avec seulement gradle."

#: pages/documentation.scm:36
msgid ""
"To download a dictionary, you need to click on the three dots\n"
"at the top right of the screens and select “Manage Dictionaries”. This will\n"
"open a new view where you can select a dictionary in a list."
msgstr ""
"Pour télécharger un dictionnaire, vous devez appuyer sur les trois points en "
"haut à droite de l'écran et choisir « gérer les dictionnaires ». Cela vous "
"amènera à une nouvelle vue où vous pourrez choisir un dictionnaire dans une "
"liste."

#: tools/theme.scm:75
msgid "Translate"
msgstr "Traduire"

#: tools/theme.scm:102
msgid "View posts for every language"
msgstr "Voir les billets dans toutes les langues"

#: pages/index.scm:44
msgid "Works offline with the features you want"
msgstr "Fonctionne hors lignes avec les fonctionnalités que vous choisissez"

#: pages/documentation.scm:67
msgid "Write some code and send me a patch"
msgstr "Écrivez du code et envoyez-moi des correctifs"

#: pages/index.scm:52
msgid "You can build it, contribute to it and its data sources"
msgstr ""
"Vous pouvez la construire, contribuer à son développement et à ses sources "
"de données"

#: pages/documentation.scm:56
msgid "You can help too!"
msgstr "Vous aussi, vous pouvez aider !"

#: tools/theme.scm:94
#, scheme-format
msgid "by ~a — ~a"
msgstr "par ~a — ~a"
