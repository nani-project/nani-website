RADK_MODULES=tools/radk.scm
DICOS+=dicos/radicals.nani
DOWNLOADS+=dictionaries/radkfilex.utf8

dictionaries/radkfilex.utf8:
	#wget ftp://ftp.monash.edu/pub/nihongo/kradzip.zip -O dictionaries/kradzip.zip
	wget http://ftp.usf.edu/pub/ftp.monash.edu.au/pub/nihongo/kradzip.zip -O dictionaries/kradzip.zip
	unzip dictionaries/kradzip.zip radkfilex -d dictionaries
	iconv -f euc-jp -t utf-8 dictionaries/radkfilex > $@
	rm dictionaries/radkfilex

dicos/radicals.nani: tools/radk.scm dictionaries/radkfilex.utf8 dictionaries/kanjidic2.xml $(RADK_MODULES)
	guile -L modules tools/radk.scm build dictionaries/radkfilex.utf8 dictionaries/kanjidic2.xml $@
