WADOKU_TMP_DIR=dictionaries/wadoku-tmp
DICOS+=dicos/wadoku_ger.nani dicos/wadoku_pitch.nani
DOWNLOADS+=dictionaries/wadoku.xml

dictionaries/wadoku.xml:
	mkdir $(WADOKU_TMP_DIR)
	wget https://www.wadoku.de/downloads/xml-export/ -O $(WADOKU_TMP_DIR)/index.html
	file=$$(grep href $(WADOKU_TMP_DIR)/index.html | cut -f2 --delimiter='"' | \
        grep ^wadoku-xml | grep xz$$ | tail -1); \
    wget https://www.wadoku.de/downloads/xml-export/$$file -O \
        $(WADOKU_TMP_DIR)/wadoku.tar.xz
	(cd $(WADOKU_TMP_DIR); tar xf wadoku.tar.xz)
	cp $(WADOKU_TMP_DIR)/wadoku-xml*/wadoku.xml $@
	rm -rf $(WADOKU_TMP_DIR)

dicos/wadoku_ger.nani: dictionaries/wadoku.xml tools/wadoku.scm dictionaries/frequency.tsv $(DICO_MODULES)
	guile -L modules tools/wadoku.scm build $< $@

dicos/wadoku_pitch.nani: dictionaries/wadoku.xml tools/wadoku.scm
	guile -L modules tools/wadoku.scm pitch $< $@
