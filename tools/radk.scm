;;; Nani Project website
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of the Nani Project website.
;;;
;;; The Nani Project website is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; The Nani Project website is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with the Nani Project website.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (nani kanji radk))
(use-modules (ice-9 match))
(use-modules (ice-9 binary-ports))

(match (command-line)
  ((_ cmd radk-file kanjidic-file output)
   (cond
    ((equal? cmd "build")
     (let* ((radk (parse-radk radk-file))
            (rad-stroke (get-rad-stroke radk))
            (rad-kanji (get-rad-kanji radk))
            (kanji-stroke (get-kanji-stroke kanjidic-file)))
       (call-with-output-file output
         (lambda (port)
           (put-bytevector port
             (serialize-radk rad-kanji rad-stroke kanji-stroke))))))
    (else (format #t "Unknown cmd ~a.~%" cmd)))))
