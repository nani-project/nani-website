;;; Nani Project website
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of the Nani Project website.
;;;
;;; The Nani Project website is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; The Nani Project website is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with the Nani Project website.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (nani result frequency))
(use-modules (nani result jmdict))
(use-modules (nani result result))
(use-modules (ice-9 match))
(use-modules (ice-9 binary-ports))

;; Break these steps to try and let the GC reclaim these big objects
(define (get-results1 input frq)
  (call-with-input-file input
    (lambda (port)
      (xml->results port frq))))

(define (get-results input meaning-filter frq)
  (let* ((results (get-results1 input frq))
         (results (map (lambda (result)
                         (update-result
                           result
                           #:meanings (filter meaning-filter
                                            (result-meanings result))))
                       results))
         (results (filter (lambda (result) (not (null? (result-meanings result))))
                          results)))
    results))

(define (compile input meaning-filter output)
  (let* ((results (get-results input meaning-filter
                               (load-frequency "dictionaries/frequency.tsv"))))
    (format #t "Number of entries in ~a: ~a~%" output (length results))
    (call-with-output-file output
      (lambda (port)
        (put-bytevector port
          (serialize-dictionary results))))))

(define (print word dict)
  #t)

(match (command-line)
  ((_ cmd input lang output)
   (cond
    ((equal? cmd "build")
     (if (equal? lang "e")
       (compile input (const #t) output)
       (compile input (lambda (meaning) (equal? (meaning-language meaning) lang)) output)))
    (else (format #t "Unknown cmd ~a.~%" cmd))))
  ((_ "print" word input)
   (print word input)))
