;;; Nani Project website
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is part of the Nani Project website.
;;;
;;; The Nani Project website is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; The Nani Project website is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with the Nani Project website.  If not, see <http://www.gnu.org/licenses/>.

(define-module (tools theme)
  #:use-module (haunt builder blog)
  #:use-module (haunt post)
  #:use-module (haunt site)
  #:use-module (tools haunt-i18n)
  #:use-module (tools i18n)
  #:use-module (srfi srfi-1)
  #:export (nani-theme))

(define (nani-theme lang page)
  (theme #:name "Nani"
         #:layout
         (lambda (site title body)
           (site-locale lang)
           `((doctype "html")
             (head
               (meta (@ (charset "utf-8")))
               (meta (@ (name "viewport") (content "width=device-width, initial-scale=1")))
               (title ,(list title " — " (site-title site)))
               (link (@ (rel "stylesheet") (href "/css/nani.css")))
               (link (@ (rel "stylesheet") (href "/css/device.css"))))
             (body (@ (lang ,lang))
               (header
                 (span (a (@ (href ,(string-append (if (equal? lang "") "" "/")
                                                   lang "/")))
                          "何?"))
                 (nav (@ (id "main-menu"))
                   (label (@ (for "hamburger") (id "hamburger-label"))
                     (img (@ (alt "(open the menu)") (src "/images/hamburger.png"))))
                   (input (@ (id "hamburger") (type "checkbox")))
                   (ul
                     (li (a (@ (href ,(string-append (if (equal? lang "") "" "/")
                                                     lang "/index.html")))
                            ,(_ "Features")))
                     (li (a (@ (href ,(string-append (if (equal? lang "") "" "/")
                                                     lang "/documentation.html")))
                            ,(_ "Documentation")))
                     (li (a (@ (href "https://framagit.org/nani-project/nani-app"))
                            ,(_ "Build it")))
                     (li (@ (id "lang-selector"))
                         (p ,(_ "Language"))
                         (ul ,@(map
                                (lambda (ll)
                                  (site-locale ll)
                                  (let ((li
                                          `(li
                                             (a (@ (href ,(string-append
                                                            "/" ll "/"
                                                            (basename page)
                                                            ".html")))
                ;; TRANSLATORS: translate this with the name of your language.
                                                 ,(_ "English")))))
                                    (site-locale lang)
                                    li))
                                '("en" "fr" "uk" "zh_CN"))
                             (li (a (@ (href "https://translate.fedoraproject.org/projects/nani/"))
                                    ,(_ "Translate"))))))))
               (div (@ (id "page"))
                 (div (@ (id "content-block")) (div (@ (id "content")) ,body)))
               (footer
                 (div (@ (class "footer-flex"))
                   (p "© Copyright Julien Lepiller 2019")
                   (p (a (@ (href ,(string-append (if (equal? lang "") "" "/")
                                                  lang "/mentions.html")))
                         ,(_ "Legal notices"))))
                 (p ,(_ "The source of this website can be seen at
<a href=\"~a\">framagit</a>. This website is free software; you can redistribute
it and/or modify it under the terms of the GNU Affero General Public License
as published by the Free Software Foundation; either version 3 of the License,
or (at your option) any later version."
                    "https://framagit.org/nani-project/nani-website"))))))
         #:post-template
         (lambda (post)
           `((article
             (h1 ,(post-ref post 'title))
             (p (@ (class "date")) ,(_ "by ~a — ~a" (post-ref post 'author) (date->string* (post-date post))))
             (div ,(post-sxml post)))))
         #:collection-template
         (lambda (site title posts prefix)
           (define (post-uri post)
             (string-append "/" (or prefix "")
                            (site-post-slug site post) ".html"))
           `(div
              (p (a (@ (href "/blog-complete.html")) ,(_ "View posts for every language")))
              (div (@ (id "post-list"))
                ,@(map (lambda (post)
                         (let ((language (fold (lambda (key acc) (if (member key (post-ref post 'tags)) key acc)) "" languages)))
                           `(div (@ (class ,(string-append "post " language)))
                              (p (@ (class "title"))
                                 (a (@ (href ,(post-uri post)))
                                    ,(post-ref post 'title)))
                              (p (@ (class "post-content"))
                                 ,(post-ref post 'summary))
                              (p (@ (class "date"))
                                 (span
                                   (a (@ (href ,(post-uri post)))
                                        ,(_ "Read")))
                                 (span (@ (class ,language)) ,language)
                                 ,(date->string* (post-date post))))))
                    posts))))))
