TATOEBA_LANGS=eng fra rus spa ukr
DICOS+=$(addprefix dicos/tatoeba_, $(addsuffix .nani, $(TATOEBA_LANGS)))
TATOEBA_DOWNLOADS+=$(addprefix dictionaries/tatoeba_, $(addsuffix .csv, sentences_detailed sentences_base tags user_languages))
DOWNLOADS+=$(TATOEBA_DOWNLOADS)

.PRECIOUS: dictionaries/tatoeba%.csv

dictionaries/tatoeba%.csv:
	wget https://downloads.tatoeba.org/exports/$$(basename $@ .csv | cut -c9-).tar.bz2 -O $@.tar.bz2 --continue
	tar xf $@.tar.bz2 -C dictionaries
	mv dictionaries/$$(basename $@ | cut -c9-) $@

dicos/tatoeba_%.nani: $(TATOEBA_DOWNLOADS) $(TATOEBA_MODULES) $(RADK_MODULES)
	guile -L modules tools/tatoeba.scm $(shell basename $@ .nani | sed 's|^tatoeba_||g') $@
